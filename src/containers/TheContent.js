import React, { Suspense, useContext } from 'react'
import {
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import { CContainer, CFade } from '@coreui/react'
import { UserContext } from '../context';

// routes config
import routes from '../routes'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

const filterRoutesByAuthLevel = (route, userInfo) => {
  switch (route.authLevel) {
    case 'admin': {
      return !route.requireAdmin || (route.requireAdmin && userInfo.isAdmin)
    }
    case 'roomPrivilege': {
      return !route.roomPrivilegeRequired || (route.roomPrivilegeRequired === userInfo.roomPrivilege)
    }
    case 'reportPrivilege': {
      return !route.reportPrivilegeRequired || (route.reportPrivilegeRequired === userInfo.roomPrivilege)
    }
    case 'KPIPrivilege': {
      return !route.KPIPrivilegeRequired || (route.KPIPrivilegeRequired === userInfo.KPIPrivilege)
    }
    default: {
      return true;
    }
  }
}

const TheContent = () => {
  const { userInfo } = useContext(UserContext);
  return (
    <main className="c-main">
      <CContainer fluid>
        <Suspense fallback={loading}>
          <Switch>
            {routes.filter((route) => filterRoutesByAuthLevel(route, userInfo))
              .map((route, idx) => {
                return route.component && (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (
                      <CFade>
                        <route.component {...props} />
                      </CFade>
                    )} />
                )
              })}
            <Redirect from="/" to="/dashboard" />
          </Switch>
        </Suspense>
      </CContainer>
    </main>
  )
}

export default React.memo(TheContent)
