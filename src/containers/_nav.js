import React from "react";
import CIcon from "@coreui/icons-react";

const _nav = [
  {
    _tag: "CSidebarNavItem",
    name: "Dashboard",
    to: "/dashboard",
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon" />,
  },

  // {
  //   _tag: 'CSidebarNavTitle',
  //   _children: ['']
  // },
  {
    _tag: "CSidebarNavItem",
    name: "Users",
    to: "/users",
    icon: "cil-user",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Staffs",
    to: "/Staffs",
    icon: "cil-people",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Teams",
    to: "/Teams",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        class="c-sidebar-nav-icon"
        role="img"
      >
        <rect
          width="288"
          height="32"
          x="112"
          y="152"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <rect
          width="288"
          height="32"
          x="112"
          y="240"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <rect
          width="152"
          height="32"
          x="112"
          y="328"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <path
          fill="var(--ci-primary-color, currentColor)"
          d="M472,48H40A24.028,24.028,0,0,0,16,72V440a24.028,24.028,0,0,0,24,24h88V405.178A20.01,20.01,0,0,1,138.284,387.7l91.979-51.123L200,260.919V200a56,56,0,0,1,112,0v60.919l-30.263,75.655L373.716,387.7h0A20.011,20.011,0,0,1,384,405.178V464h88a24.028,24.028,0,0,0,24-24V72A24.028,24.028,0,0,0,472,48Zm-8,384H416V405.178a52.027,52.027,0,0,0-26.738-45.451h0L321.915,322.3,344,267.081V200a88,88,0,0,0-176,0v67.081L190.085,322.3l-67.347,37.432A52.027,52.027,0,0,0,96,405.178V432H48V80H464Z"
          class="ci-primary"
        ></path>
      </svg>
    ),
  },
  {
    _tag: "CSidebarNavItem",
    name: "Reports",
    to: "/Reports",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        class="c-sidebar-nav-icon"
        role="img"
      >
        <rect
          width="288"
          height="32"
          x="112"
          y="152"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <rect
          width="288"
          height="32"
          x="112"
          y="240"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <rect
          width="152"
          height="32"
          x="112"
          y="328"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <path
          fill="var(--ci-primary-color, currentColor)"
          d="M88,72V388a20,20,0,0,1-40,0V152H16V388a52.059,52.059,0,0,0,52,52H444a52.059,52.059,0,0,0,52-52V72ZM464,388a20.023,20.023,0,0,1-20,20H116a51.722,51.722,0,0,0,4-20V104H464Z"
          class="ci-primary"
        ></path>
      </svg>
    ),
  },
  {
    _tag: "CSidebarNavItem",
    name: "Activities",
    to: "/Activities",
    icon: <CIcon name="cil-notes" customClasses="c-sidebar-nav-icon" />,
  },
  {
    _tag: "CSidebarNavItem",
    name: "Data Room",
    to: "/DataRoom",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        class="c-sidebar-nav-icon"
        role="img"
      >
        <rect
          width="288"
          height="32"
          x="112"
          y="152"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <rect
          width="288"
          height="32"
          x="112"
          y="240"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <rect
          width="152"
          height="32"
          x="112"
          y="328"
          fill="var(--ci-primary-color, currentColor)"
          class="ci-primary"
        ></rect>
        <path
          fill="var(--ci-primary-color, currentColor)"
          d="M488.671,221.645a23.848,23.848,0,0,0-18.917-9.231h-66.2V160a23.138,23.138,0,0,0-23.112-23.111h-136.3L226.37,94.22A23.051,23.051,0,0,0,205.037,80H39.111A23.138,23.138,0,0,0,16,103.111V467.556H436.707l56.33-225.321A23.849,23.849,0,0,0,488.671,221.645ZM48,435.556V112H199.111l23.7,56.889H371.556v43.525H137.587A23.965,23.965,0,0,0,114.3,230.593L63.063,435.556Zm363.723,0H96.048l47.785-191.142H459.508Z"
          class="ci-primary"
        ></path>
      </svg>
    ),
  },
];

export default _nav;
