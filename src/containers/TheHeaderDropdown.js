import React, { useContext } from "react";
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { UserContext } from "src/context";

const TheHeaderDropdown = () => {
  const { userInfo, logOut } = useContext(UserContext);
  const { firstName, lastName } = userInfo;
  console.log("::", firstName);
  const getInitialChar = () =>
    firstName.split("")[0].toUpperCase() + lastName.split("")[0].toUpperCase();
  return (
    <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar orange">{getInitialChar()}</div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem header tag="div" color="light" className="text-center">
          <strong>Settings</strong>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-user" className="mfe-2" />
          Profile
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-settings" className="mfe-2" />
          Settings
        </CDropdownItem>
        <CDropdownItem onClick={logOut}>
          <CIcon name="cilAccountLogout" className="mfe-2" />
          Logout
        </CDropdownItem>
        <CDropdownItem divider />
      </CDropdownMenu>
    </CDropdown>
  );
};

export default TheHeaderDropdown;
