/**
 * Transform Data for API Specific format
 * @param {*} data
 * @returns
 */
export const transformForRequest = ({ phoneCode, ...rest }) => {
  const transformmedData = {
    ...rest,
    phoneNumber: `${phoneCode}${rest.phoneNumber}`,
  };
  return transformmedData;
};

/**
 * Transform Data from API into form data
 * @param {*} data
 * @returns
 */
export const transformFromRequest = ({
  createdAt,
  description,
  id,
  originalId,
  reportName,
  reportTypeBy,
  reportTypeId,
  frequency,
  reportStartDate,
  status,
  gracePeriod,
}) => {
  const transformmedData = {
    createdAt,
    description,
    id,
    originalId,
    reportName,
    reportTypeBy,
    reportTypeId,
    frequency,
    reportStartDate: reportStartDate.includes("T")
      ? reportStartDate.split("T")[0]
      : reportStartDate,
    status,
    gracePeriod,
  };
  console.log(":::>>>>>>>>>>>", transformmedData);
  return transformmedData;
};
