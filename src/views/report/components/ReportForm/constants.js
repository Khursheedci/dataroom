export const initialForm = {
  description: undefined,
  reportName: undefined,
  reportTypeBy: 1,
  reportTypeId: undefined,
  frequency: undefined,
  reportStartDate: undefined,
  status: undefined,
  gracePeriod: undefined,
};

export const editInitialForm = {
  createdAt: true,
  description: true,
  id: true,
  originalId: true,
  reportName: true,
  reportTypeBy: true,
  reportTypeId: true,
  frequency: true,
  reportStartDate: true,
  status: true,
  gracePeriod: true,
};

export const phoneCodes = [
  23225, 23230, 23231, 23232, 23233, 23234, 23244, 23275, 23276, 23277, 23278,
  23279, 23280, 23288, 23299,
];
export const statusOptions = [
  { name: "Pending", value: 1 },
  { name: "Submitted", value: 2 },
  { name: "Defaulted", value: 3 },
];
export const frequencyOptions = [
  { name: "Weekly", value: 1 },
  { name: "Monthly", value: 2 },
  { name: "Quarterly", value: 3 },
  { name: "Half Yearly", value: 4 },
  { name: "Annually", value: 5 },
];
export const otherPrivOptions = [
  { name: "Staff", value: 1 },
  { name: "Team", value: 2 },
];
