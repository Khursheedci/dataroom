import React, { useState } from "react";
import {
  CDataTable,
  CCollapse,
  CCardBody,
  CRow,
  CCol,
  CCallout,
  CBadge,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import { element } from "prop-types";
import { date } from "yup/lib/locale";
import NoItemView from "../../../noItemView/noItemView";

const fields = [
  { key: "reportName", label: "Report Name" },
  { key: "description", label: "Description" },
  { key: "frequencyText", label: "Frequency" },
  { key: "gracePeriodText", label: "Grace Period" },
  { key: "Actions", label: "Actions" },
];
const roomPrivOptions = [
  { label: "Board", value: 1 },
  { label: "Senior Management Cabinet", value: 2 },
  { label: "Departmental Cabinet", value: 3 },
];
const otherPriv = {
  1: "Draft",
  2: "Final",
};

const ReportTable = ({ userList, setUserEdit, setUserDelete, setShowbtn }) => {
  const [details, setDetails] = useState([]);

  userList.forEach(
    (e) =>
      (e.reportStartDate = e.reportStartDate.includes("T")
        ? e.reportStartDate.split("T")[0]
        : e.reportStartDate)
  );
  userList.forEach((e) => {
    e.gracePeriodText =
      e.gracePeriod !== 0
        ? e.gracePeriod.toString() + " Days"
        : "NO Grace Period";
  });
  userList.forEach((e) => {
    let freq = 0;
    if (e.frequency === 1) {
      freq = 7;
      e.frequencyText = "Weekly";
    } else if (e.frequency === 2) {
      freq = 30;
      e.frequencyText = "Monthly";
    } else if (e.frequency === 3) {
      freq = 90;
      e.frequencyText = "Quarterly";
    } else if (e.frequency === 4) {
      freq = 180;
      e.frequencyText = "Half Yearly";
    } else if (e.frequency === 5) {
      freq = 360;
      e.frequencyText = "Annually";
    } else {
      e.frequencyText = "Not Applicable";
    }
    const dt = new Date(e.reportStartDate);
    dt.setDate(dt.getDate() + freq);
    e.reportEndDate = dt.toJSON().slice(0, 10);
  });
  const showProfile = (item) => {
    setShowbtn(true);
    setUserEdit(item);
  };
  const showEdit = (item) => {
    setShowbtn(false);
    setUserEdit(item);
  };

  const toggleDetails = ({ originalId: id }) => {
    const position = details.indexOf(id);
    let newDetails = details.slice();
    if (position !== -1) {
      newDetails.splice(position, 1);
    } else {
      newDetails.push(id);
    }
    setDetails(newDetails);
  };

  const actions = [
    { icon: freeSet.cilLifeRing, onClick: showProfile },
    { icon: freeSet.cilPenAlt, onClick: showEdit },
    { icon: freeSet.cilUserX, onClick: setUserDelete },
  ];

  return (
    <CDataTable
      bordered={true}
      items={userList}
      fields={fields}
      itemsPerPage={5}
      pagination
      tableFilter={{ placeholder: "search..." }}
      noItemsViewSlot={<NoItemView />}
      scopedSlots={{
        Actions: (item) => (
          <div>
            {actions.map((action) => (
              <td>
                <CIcon
                  size={"lg"}
                  content={action.icon}
                  onClick={() => action.onClick(item)}
                />
              </td>
            ))}
          </div>
        ),
      }}
    />
  );
};

export default ReportTable;
