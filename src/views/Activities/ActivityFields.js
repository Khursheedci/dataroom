const fields = [
  {
    key: "name",
    label: "Activity Name",
    _style: { width: "25%" },
    sorter: false,
    filter: false,
  },
  {
    key: "startDate",
    label: "Start Date",
    _style: { width: "10%", textAlign: "center" },
    sorter: false,
    filter: false,
  },
  {
    key: "endDate",
    label: "End Date",
    _style: { width: "10%", textAlign: "center" },
    sorter: false,
    filter: false,
  },
  {
    key: "progress",
    label: "Progress",
    _style: {
      width: "25%",
      textAlign: "center",
    },
    sorter: false,
    filter: false,
  },
  {
    key: "status",
    label: "Status",
    _style: {
      width: "10%",
      textAlign: "center",
    },
    sorter: false,
    filter: false,
  },
  {
    key: "Actions",
    label: "Actions",
    _style: { width: "20%", textAlign: "center" },
    sorter: false,
    filter: false,
  },
];
export default fields;
