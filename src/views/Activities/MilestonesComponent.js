import { React, useEffect, useState } from "react";
import { CDataTable } from "@coreui/react";
import CIcon from "@coreui/icons-react";

import { freeSet } from "@coreui/icons";

import ActivityServices from "src/services/activities";
import uuid from "react-uuid";
import _ from "lodash";
import Modal from "./Modal";
import msfields from "./MilestoneFields";
const activityServices = new ActivityServices();
const MilsestonesComponent = (props) => {
  const [action, setAction] = useState("");
  const [modal, setModal] = useState(false);
  const [index, setIndex] = useState(0);
  const [data, setData] = useState({});
  const [id, setId] = useState("");

  const [activityData, setActivityData] = useState("");
  const [collapseIndex, setCollapseIndex] = useState(-1);
  const markCompleted = (idd) => {
    let data1 = {
      activityBy: props.item.activityBy,
      activityById: [...props.item.activityById],
      description: props.item.description,
      endDate: props.item.endDate.slice(0, 10),
      isCompleted: false,
      milestones: [...props.item.milestones],
      name: props.item.name,
      startDate: props.item.startDate.slice(0, 10),
    };
    let actData = { ...data1 };
    actData.milestones.find((item) => item.id === idd).status = "completed";
    activityServices
      .updateActivity(props.item.originalId, actData)
      .then((res) =>
        activityServices
          .getActivities()
          .then((res) => {
            console.log(res);
            props.sendData(res);
            props.collapse(res.length - 1);
          })
          .catch((err) => console.log(err))
      )
      .catch((err) => console.log(err));
  };
  const deleteMile = (index) => {
    let data1 = {
      activityBy: props.item.activityBy,
      activityById: [...props.item.activityById],
      description: props.item.description,
      endDate: props.item.endDate.slice(0, 10),
      isCompleted: false,
      milestones: [...props.item.milestones],
      name: props.item.name,
      startDate: props.item.startDate.slice(0, 10),
    };
    let actData = { ...data1 };
    actData.milestones.splice(index, 1);
    activityServices
      .updateActivity(props.item.originalId, actData)
      .then((res) =>
        activityServices
          .getActivities()
          .then((res) => {
            console.log(res);
            props.sendData(res);
            props.collapse(res.length - 1);
          })
          .catch((err) => console.log(err))
      )
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    props.passData(activityData);
  }, [activityData]);
  useEffect(() => {
    props.collapse(collapseIndex);
  }, [collapseIndex]);
  return (
    <div>
      <Modal
        open={modal}
        type="Milestone"
        action={action}
        toggle={() => setModal(false)}
        data={data}
        index={index}
        activityData={props.item}
        id={id}
        sendData={(data) => setActivityData(data)}
        collapse={(val) => setCollapseIndex(val)}
      />

      <CDataTable
        items={props.item.milestones}
        fields={msfields}
        hover
        sorter
        pagination
        scopedSlots={{
          name: (item) => (
            <td
              style={{
                textAlign: "center",
                verticalAlign: "middle",
                fontSize: "12",
                fontWeight: 700,
              }}
            >
              {item.name}
            </td>
          ),
          startDate: (item) => (
            <td
              style={{
                textAlign: "center",
                verticalAlign: "middle",
              }}
            >
              {item.startDate}
            </td>
          ),
          endDate: (item) => (
            <td
              style={{
                textAlign: "center",
                verticalAlign: "middle",
              }}
            >
              {item.endDate}
            </td>
          ),
          contribution: (item) => (
            <td
              style={{
                verticalAlign: "middle",
                width: "15%",
                textAlign: "center",
              }}
            >
              {`${item.contribution}%`}
            </td>
          ),
          Actions: (item, index) => {
            return (
              <td
                classImage="py-2"
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  cursor: "pointer",
                }}
              >
                <CIcon
                  content={freeSet.cilTrash}
                  onClick={() => {
                    deleteMile(index);
                  }}
                />
                <CIcon
                  content={freeSet.cilPenAlt}
                  onClick={() => {
                    setAction("Update");
                    setModal(true);
                    setData(item);
                    setIndex(index);
                    setId(item.id);
                  }}
                />
                {item.status !== "completed" ? (
                  <CIcon
                    name="cil-check"
                    onClick={() => {
                      markCompleted(item.id);
                    }}
                  />
                ) : (
                  <div style={{ width: 15 }}></div>
                )}
              </td>
            );
          },
        }}
      />
    </div>
  );
};

export default MilsestonesComponent;
