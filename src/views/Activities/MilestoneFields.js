const msfields = [
  {
    key: "name",
    label: "Milestone Name",
    _style: {
      width: "25%",
      textAlign: "center",
      background: "#636f83",
      color: "#fafafa",
    },
    sorter: false,
    filter: false,
  },
  {
    key: "contribution",
    label: "Contribution",
    _style: {
      width: "25%",
      textAlign: "center",
      background: "#636f83",
      color: "#fafafa",
    },
    sorter: false,
    filter: false,
  },
  {
    key: "startDate",
    label: "Start Date",
    _style: {
      width: "20%",
      textAlign: "center",
      background: "#636f83",
      color: "#fafafa",
    },
    sorter: false,
    filter: false,
  },
  {
    key: "endDate",
    label: "End Date",
    _style: {
      width: "20%",
      textAlign: "center",
      background: "#636f83",
      color: "#fafafa",
    },
    sorter: false,
    filter: false,
  },
  {
    key: "Actions",
    label: "Actions",
    _style: {
      width: "10%",
      textAlign: "center",
      background: "#636f83",
      color: "#fafafa",
    },
    sorter: false,
    filter: false,
  },
];

export default msfields;
