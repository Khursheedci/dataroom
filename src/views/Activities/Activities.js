import { React, useEffect, useState } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CCollapse,
  CDataTable,
  CProgress,
  CBadge,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import uuid from "react-uuid";

import { freeSet } from "@coreui/icons";
import MilsestonesComponent from "./MilestonesComponent";
import Modal from "./Modal";
import fields from "./ActivityFields";
import activityData from "./ActivityData";
import ActivityServices from "src/services/activities";
import { Math } from "core-js";
const activityServices = new ActivityServices();
const Activities = (props) => {
  const [details, setDetails] = useState(-1);
  const [action, setAction] = useState("");
  const [type, setType] = useState("");
  const [modal, setModal] = useState(false);
  const [ActivityData, setActivityData] = useState([]);
  const [id, setId] = useState("");
  const [item1, setItem1] = useState({
    name: "",
    activityBy: 0,
    activityById: [],
    startDate: "",
    endDate: "",
    milestones: [],
    description: "",
    isCompleted: false,
  });

  useEffect(() => {
    activityServices
      .getActivities()
      .then((res) => {
        console.log(res);
        setActivityData(res);
      })
      .catch((err) => console.log(err));
  }, []);
  const deleteActivity = (id) => {
    activityServices
      .deleteActivity(id)
      .then((res) => {
        console.log(res);
        activityServices
          .getActivities()
          .then((res) => {
            console.log(res);
            setActivityData(res);
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log(err));
  };
  const checkMarked = (item, idd) => {
    console.log(item);
    let data = { ...item };
    let data1 = {
      activityBy: data.activityBy,
      activityById: [...data.activityById],
      description: data.description,
      endDate: data.endDate.slice(0, 10),
      isCompleted: true,
      milestones: [...data.milestones],
      name: data.name,
      startDate: data.startDate.slice(0, 10),
    };
    console.log(data1);

    activityServices
      .updateActivity(idd, data1)
      .then((res) => {
        console.log(res);
        activityServices
          .getActivities()
          .then((res) => {
            console.log(res);
            setActivityData(res);
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log(err));
  };
  const toggleDetails = (index) => {
    if (details === index) {
      setDetails(-1);
    } else {
      setDetails(index);
    }
  };

  const calcProgress = (data) => {
    let progress = 0;
    if (data.milestones.length !== 0) {
      for (let i = 0; i <= data.milestones.length; i++) {
        if (data.milestones[i] && data.milestones[i].status === "completed") {
          progress = progress + Number(data.milestones[i].contribution);
        }
      }
      return progress;
    } else if (data.isCompleted) {
      return 100;
    } else return "undefined";
  };
  const getBadge = (data) => {
    if (data === 100) {
      return "success";
    } else return "warning";
  };
  const getProgressColor = (data) => {
    if (calcProgress(data) < 50) {
      return "danger";
    } else if (calcProgress(data) > 50 && calcProgress(data) < 75) {
      return "warning";
    } else return "success";
  };
  return (
    <CCard>
      <Modal
        id={id}
        activityData={item1}
        open={modal}
        type={type}
        action={action}
        toggle={() => setModal(false)}
        sendData={(data) => {
          setActivityData(data);
          console.log(data);
        }}
        collapse={(val) => setDetails(val)}
      />

      <CCardHeader>
        <CCol col="12" sm="12" md="12" xl>
          <CButton
            onClick={() => {
              // if (action !== "Update") {
              //   setItem1();
              // }

              setAction("Create");
              setType("Activity");
              setModal(true);
              setId("");
            }}
            style={{ float: "right" }}
            active
            color="success"
            aria-pressed="true"
          >
            Add Activity
          </CButton>
        </CCol>
      </CCardHeader>
      <CCardBody style={{ padding: 5 }}>
        <CDataTable
          items={ActivityData}
          fields={fields}
          itemsPerPageSelect
          itemsPerPage={5}
          hover
          tableFilter={{ placeholder: "search..." }}
          sorter
          pagination
          scopedSlots={{
            name: (item) => (
              <td
                style={{
                  marginLeft: "1%",
                  verticalAlign: "middle",
                  fontSize: "12",
                  fontWeight: 700,
                }}
              >
                {item.name}
              </td>
            ),
            startDate: (item) => (
              <td style={{ textAlign: "center", verticalAlign: "middle" }}>
                {item.startDate.slice(0, 10)}
              </td>
            ),
            endDate: (item) => (
              <td style={{ textAlign: "center", verticalAlign: "middle" }}>
                {item.endDate.slice(0, 10)}
              </td>
            ),
            progress: (item) => (
              <td style={{ verticalAlign: "middle", width: "15%" }}>
                {calcProgress(item) !== "undefined" &&
                item.milestones.length !== 0 ? (
                  <CProgress
                    color={getProgressColor(item)}
                    value={calcProgress(item)}
                    // showValue
                    className="mb-1 bg-white"
                    height="30px"
                    striped
                    showPercentage
                  />
                ) : (
                  <div
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <CBadge color={getBadge(calcProgress(item))} width="200%">
                      No Milestones
                    </CBadge>
                  </div>
                )}
              </td>
            ),

            status: (item) => (
              <td
                style={{
                  verticalAlign: "middle",
                  width: "15%",
                  textAlign: "center",
                }}
              >
                <CBadge color={getBadge(calcProgress(item))}>
                  {calcProgress(item) === 100
                    ? "Completed"
                    : calcProgress(item) === "undefined"
                    ? "Pending"
                    : "Pending"}
                </CBadge>
              </td>
            ),
            Actions: (item, index) => {
              return (
                <td
                  classImage="py-2"
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    cursor: "pointer",
                  }}
                >
                  {}

                  {item.milestones.length === 0 &&
                  item.isCompleted === false ? (
                    <CIcon
                      name="cil-check"
                      onClick={() => {
                        checkMarked(item, item.originalId);
                      }}
                    />
                  ) : (
                    <div style={{ width: 15 }}></div>
                  )}
                  <CIcon
                    content={freeSet.cilTrash}
                    onClick={() => deleteActivity(item.originalId)}
                  />

                  <CIcon
                    content={freeSet.cilPenAlt}
                    onClick={() => {
                      setId(item.originalId);
                      setAction("Update");
                      setType("Activity");
                      setItem1(item);
                      setModal(true);
                    }}
                  />
                  <CIcon
                    content={freeSet.cilLibraryAdd}
                    onClick={() => {
                      setId(item.originalId);
                      setAction("Create");
                      setType("Milestone");
                      setItem1(item);
                      setTimeout(() => setModal(true), 100);
                      // setNewMileStoneId(uuid());
                    }}
                  />

                  <CIcon
                    name={
                      details === index ? "cilChevronTop" : "cilChevronBottom"
                    }
                    onClick={() => {
                      toggleDetails(index);
                    }}
                  />
                </td>
              );
            },
            details: (item, index) => (
              <CCollapse show={details === index}>
                <div style={{ padding: "10px 30px", background: "#ebedef" }}>
                  <MilsestonesComponent
                    item={item}
                    index={index}
                    details={details}
                    activityData={ActivityData}
                    sendData={(data) => {
                      setActivityData(data);
                      console.log(data);
                    }}
                    passData={(data) => {
                      setActivityData(data);
                      console.log(data);
                    }}
                    id={id}
                    collapse={(val) => setDetails(val)}
                  />
                </div>
              </CCollapse>
            ),
          }}
        />
      </CCardBody>
    </CCard>
  );
};

export default Activities;
