import React, {
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
} from "react";
import {
  CButton,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  CRow,
} from "@coreui/react";
import _ from "lodash";

const MilestoneForm = forwardRef((props, ref) => {
  const [id, setId] = useState("");
  const [random, setRandom] = useState({
    id: "",
    name: "",
    startDate: "",
    endDate: "",
    contribution: "",
    status: "active",
  });

  useEffect(() => {
    if (props.mileData) {
      console.log(props.mileData);
      setRandom((prevObj) => ({
        ...prevObj,
        name: props.mileData.name,
        id: props.mileData.id,
        startDate: props.mileData.startDate,
        endDate: props.mileData.endDate,
        contribution: props.mileData.contribution,
        status: props.mileData.status,
      }));
    }
  }, [props.mileData]);

  useEffect(() => {
    props.passData(random, props.id);
  }, [random]);
  const clearState = () => {
    setRandom((obj) => ({
      ...obj,
      id: "",
      name: "",
      startDate: "",
      endDate: "",
      contribution: "",
      status: "active",
    }));
  };

  useImperativeHandle(ref, () => {
    return {
      clearState: clearState,
    };
  });
  return (
    <CRow>
      <CCol lg="12" xs="12">
        <CForm className="form-horizontal">
          <CFormGroup row>
            <CCol xs="12" md="12" lg="12">
              <CCol xs="12" md="12" lg="12" style={{ marginTop: "1%" }}>
                <CLabel htmlFor="milestone-name"> Milestone Name </CLabel>
              </CCol>
              <CCol xs="12" md="12" lg="12">
                <CInput
                  id="milestone-name"
                  name="milestone-name"
                  placeholder="Enter Milestone Name"
                  value={random.name}
                  onChange={(e) => {
                    setRandom((prevObj) => ({
                      ...prevObj,
                      name: e.target.value,
                    }));
                  }}
                />
              </CCol>
            </CCol>
          </CFormGroup>
        </CForm>
      </CCol>
      <CCol lg="12" xs="12">
        <CForm className="form-horizontal">
          <CFormGroup row>
            <CCol xs="12" md="12" lg="12">
              <CCol xs="12" md="12" lg="12" style={{ marginTop: "1%" }}>
                <CLabel htmlFor="contribution"> Contribution </CLabel>
              </CCol>
              <CCol xs="12" md="12" lg="12">
                <CInput
                  id="contribution"
                  name="contribution"
                  placeholder="Enter Contribution"
                  value={random.contribution}
                  onChange={(e) => {
                    setRandom((prevObj) => ({
                      ...prevObj,
                      contribution: e.target.value,
                    }));
                  }}
                />
              </CCol>
            </CCol>
          </CFormGroup>
        </CForm>
      </CCol>
      <CCol lg="12" xs="12">
        <CForm className="form-horizontal">
          <CFormGroup row>
            <CCol xs="6" md="6" lg="6">
              <CCol xs="12" md="12" lg="12" style={{ marginTop: "1%" }}>
                <CLabel htmlFor="start-date"> Start Date </CLabel>
              </CCol>
              <CCol xs="12" md="12" lg="12">
                <CInput
                  type="date"
                  id="fecha-hasta"
                  name="fecha-hasta"
                  placeholder="date"
                  value={random.startDate}
                  onChange={(e) => {
                    setRandom((prevObj) => ({
                      ...prevObj,
                      startDate: e.target.value,
                    }));
                  }}
                />
              </CCol>
            </CCol>
            <CCol xs="6" md="6" lg="6">
              <CCol xs="12" md="12" lg="12" style={{ marginTop: "1%" }}>
                <CLabel htmlFor="end-date"> End Date </CLabel>
              </CCol>
              <CCol xs="12" md="12" lg="12">
                <CInput
                  type="date"
                  id="fecha-hasta"
                  name="fecha-hasta"
                  placeholder="date"
                  value={random.endDate}
                  onChange={(e) => {
                    setRandom((prevObj) => ({
                      ...prevObj,
                      endDate: e.target.value,
                    }));
                  }}
                />
              </CCol>
            </CCol>
          </CFormGroup>
        </CForm>
      </CCol>
    </CRow>
  );
});

export default MilestoneForm;
