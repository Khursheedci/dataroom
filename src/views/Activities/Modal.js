import { React, useState, useRef } from "react";
import {
  CButton,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CModal,
} from "@coreui/react";
import ActivityForm from "./ActivityForm";
import MilestoneForm from "./MilestoneForm";
import ActivityServices from "src/services/activities";
import uuid from "react-uuid";
import _ from "lodash";
const activityServices = new ActivityServices();

const Modal = (props) => {
  const [activityData, setActivityData] = useState({});
  const [id, setId] = useState("");

  const [mileData, setMileData] = useState({
    id: "",
    name: "",
    startDate: "",
    endDate: "",
    contribution: "",
    status: "active",
  });

  const create = () => {
    if (props.action === "Create" && props.type === "Activity") {
      activityServices
        .createActivity(activityData)
        .then((res) =>
          activityServices
            .getActivities()
            .then((res) => {
              props.sendData(res);
              props.collapse(res.length - 1);
            })
            .catch((err) => console.log(err))
        )
        .catch((err) => console.log(err));
    } else {
      console.log(activityData);
      activityServices
        .updateActivity(props.id, activityData)
        .then((res) =>
          activityServices
            .getActivities()
            .then((res) => {
              props.sendData(res);
              props.collapse(res.length - 1);
            })
            .catch((err) => console.log(err))
        )
        .catch((err) => console.log(err));
    }
  };

  const createMile = (mileData) => {
    let data = { ...mileData };
    data.id = uuid();
    let data1 = {
      activityBy: props.activityData.activityBy,
      activityById: [...props.activityData.activityById],
      description: props.activityData.description,
      endDate: props.activityData.endDate.slice(0, 10),
      isCompleted: false,
      milestones: [...props.activityData.milestones],
      name: props.activityData.name,
      startDate: props.activityData.startDate.slice(0, 10),
    };
    let actData = { ...data1 };

    actData.milestones.splice(actData.milestones.length, 0, data);

    activityServices
      .updateActivity(props.activityData.originalId, actData)
      .then((res) =>
        activityServices
          .getActivities()
          .then((res) => {
            console.log(res);
            props.sendData(res);
            props.collapse(res.length - 1);
          })
          .catch((err) => console.log(err))
      )
      .catch((err) => console.log(err));
  };

  const editMile = (mileData) => {
    let data = { ...mileData };

    data.id = uuid();

    let data1 = {
      activityBy: props.activityData.activityBy,
      activityById: [...props.activityData.activityById],
      description: props.activityData.description,
      endDate: props.activityData.endDate.slice(0, 10),
      isCompleted: false,
      milestones: [...props.activityData.milestones],
      name: props.activityData.name,
      startDate: props.activityData.startDate.slice(0, 10),
    };
    let actData = { ...data1 };
    actData.milestones.splice(props.index, 1, data);
    activityServices
      .updateActivity(props.activityData.originalId, actData)
      .then((res) =>
        activityServices
          .getActivities()
          .then((res) => {
            console.log(res);
            props.sendData(res);
            props.collapse(res.length - 1);
          })
          .catch((err) => console.log(err))
      )
      .catch((err) => console.log(err));
  };

  const ref = useRef(null);

  const handleClick = () => {
    ref.current.clearState();
  };
  return (
    <div>
      {console.log(props.activityData)}
      <CModal
        show={props.open}
        onClose={() => {
          props.toggle();
          handleClick();
        }}
      >
        <CModalHeader closeButton>
          <h5>
            {props.action} {props.type}
          </h5>
        </CModalHeader>
        <CModalBody>
          {props.type === "Activity" ? (
            <ActivityForm
              passData={(data, id) => {
                setActivityData(data);
                console.log(data);
              }}
              id={props.id}
              data={props.activityData}
              action={props.action}
              ckeckId={props.checkId}
              ref={ref}
            />
          ) : (
            <MilestoneForm
              passData={(data, id) => {
                setMileData(data);
              }}
              id={props.id}
              data={props.activityData}
              action={props.action}
              mileData={props.data}
              ref={ref}
            />
          )}
        </CModalBody>
        <CModalFooter>
          <CButton
            color="primary"
            onClick={() => {
              if (props.type === "Activity") {
                create(activityData);
              } else {
                if (props.action === "Update") {
                  editMile(mileData);
                } else {
                  createMile(mileData);
                }
              }
              handleClick();
              props.toggle();
            }}
          >
            {props.action} {props.type}
          </CButton>
          <CButton
            color="secondary"
            onClick={() => {
              props.toggle();
              handleClick();
            }}
          >
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </div>
  );
};
export default Modal;
