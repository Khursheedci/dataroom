import {
  React,
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from "react";
import {
  CButton,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  CRow,
  CCardBody,
  CCard,
} from "@coreui/react";
import TeamServices from "src/services/teams";
import StaffServices from "../../services/staffs";
import ActivityServices from "src/services/activities";
const teamservices = new TeamServices();
const activityServices = new ActivityServices();
const staffservices = new StaffServices();
const ActivityForm = forwardRef((props, ref) => {
  const [owner, setOwner] = useState("Select Owner");
  const [ownerValues, setOwnerValues] = useState([]);
  const [obj, setObj] = useState({
    name: "",
    activityBy: 0,
    activityById: [],
    startDate: "",
    endDate: "",
    milestones: [],
    description: "",
    isCompleted: false,
  });
  useEffect(() => {
    if (obj.activityBy == 2) {
      teamservices
        .getUsers()
        .then((res) => {
          setOwnerValues(res);
          console.log(res);
        })
        .catch((err) => console.log(err));
    } else if (obj.activityBy == 1) {
      staffservices
        .getUsers()
        .then((res) => {
          setOwnerValues(res);
          console.log(res);
        })
        .catch((err) => console.log(err));
    }
  }, [obj.activityBy]);
  useEffect(() => {
    props.passData(obj, props.id);
  }, [obj]);
  useEffect(() => {
    if (props.id !== "" && props.data !== "undefined") {
      console.log(props.data);
      setObj((obj) => ({
        ...obj,
        activityBy: props.data.activityBy,
        isCompleted: props.data.isCompleted,
        startDate: props.data.startDate.slice(0, 10),
        endDate: props.data.endDate.slice(0, 10),
        name: props.data.name,
        activityById: props.data.activityById,
        description: props.data.description,
        milestones: [...props.data.milestones],
      }));
    }
  }, [props.id]);

  const clearState = () => {
    setObj((obj) => ({
      ...obj,
      name: "",
      activityBy: 0,
      activityById: [],
      startDate: "",
      endDate: "",
      milestones: [],
      description: "",
      isCompleted: false,
    }));
  };

  useImperativeHandle(ref, () => {
    return {
      clearState: clearState,
    };
  });
  return (
    <>
      <CRow>
        <CCol xl="12" md="12">
          {console.log(obj.activityBy)}
          <CForm className="form-horizontal">
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="type"> Type </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      custom
                      name="department"
                      id="department"
                      placeholder="Please Select"
                      value={obj.activityBy}
                      onChange={(e) => {
                        console.log(e.target.value);
                        setOwner(e.target.value);
                        setObj((prevObj) => ({
                          ...prevObj,
                          activityBy: e.target.value,
                        }));
                      }}
                    >
                      <option value={0} disabled>
                        Please Select
                      </option>
                      <option key={"Teams"} value={2}>
                        Teams
                      </option>
                      <option key={"Staffs"} value={1}>
                        Staffs
                      </option>
                    </CSelect>
                  </CCol>
                </CCol>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="team-staff"> Owner </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      custom
                      name="department"
                      id="department"
                      value={obj.activityById.length ? obj.activityById[0] : 0}
                      onChange={(e) => {
                        setOwner(e.target.value);
                        setObj((prevObj) => ({
                          ...prevObj,
                          activityById: [
                            (prevObj.activityById[0] = e.target.value),
                          ],
                        }));
                      }}
                    >
                      <option value={0} disabled disabled>
                        Please select
                      </option>
                      {console.log(ownerValues, obj.activityBy)}
                      {ownerValues.map((val, index) => (
                        <option key={index} value={val.originalId}>
                          {obj.activityBy == 2
                            ? val.teamName
                            : obj.activityBy == 1
                            ? `${val.firstName} ${val.lastName}`
                            : null}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="activity-name"> Activity Name </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      id="activity-name"
                      name="activity-name"
                      placeholder="Enter Activity Name"
                      value={obj.name}
                      onChange={(e) => {
                        setObj((prevObj) => ({
                          ...prevObj,
                          name: e.target.value,
                        }));
                      }}
                    />
                  </CCol>
                </CCol>

                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="activity-name">
                      {" "}
                      Activity Description{" "}
                    </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      id="activity-name"
                      name="activity-name"
                      placeholder="Enter Activity Description"
                      value={obj.description}
                      onChange={(e) => {
                        setObj((prevObj) => ({
                          ...prevObj,
                          description: e.target.value,
                        }));
                      }}
                    />
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="start-date"> Start Date </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      type="date"
                      id="fecha-hasta"
                      name="fecha-hasta"
                      placeholder="date"
                      value={obj.startDate}
                      onChange={(e) => {
                        setObj((prevObj) => ({
                          ...prevObj,
                          startDate: e.target.value,
                        }));
                      }}
                    />
                  </CCol>
                </CCol>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="end-date"> End Date </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      type="date"
                      id="fecha-hasta"
                      name="fecha-hasta"
                      placeholder="date"
                      value={obj.endDate}
                      onChange={(e) => {
                        setObj((prevObj) => ({
                          ...prevObj,
                          endDate: e.target.value,
                        }));
                      }}
                    />
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>
          </CForm>
        </CCol>
      </CRow>
    </>
  );
});

export default ActivityForm;
