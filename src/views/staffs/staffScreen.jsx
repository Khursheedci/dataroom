import React, { useEffect, useState } from "react";

import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
} from "@coreui/react";

import { Spinner } from "../../reusable";
import StaffTable from "./components/staffTable";
import DeleteStaff from "./components/deleteStaff";
import EditStaff from "./components/editStaff";
import StaffForm from "./components/staffForm/staffModal";
import UserServices from "../../services/staffs";

const userService = new UserServices();

const StaffScreen = () => {
  const [showUserModal, setUserModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [userList, setUserList] = useState([]);
  const [userEdit, setUserEdit] = useState(null);
  const [userDelete, setUserDelete] = useState(null);
  const [showbtn, setShowbtn] = useState(false);

  useEffect(() => console.log("shobtn is >>>>>", showbtn), [showbtn]);

  const openModal = () => {
    setShowbtn(false);
    setUserModal(true);
  };
  const closeModal = (refetch) => {
    !userEdit ? setUserModal(false) : setUserEdit(false);
    if (refetch) {
      getUsersList();
    }
  };

  const closeDeleteUser = (refetch) => {
    setUserDelete(null);
    if (refetch) {
      getUsersList();
    }
  };
  const getUsersList = () =>
    userService
      .getUsers()
      .then((res) => setUserList(res))
      .catch((err) => console.log(":::", err))
      .finally(() => setLoading(false));

  useEffect(getUsersList, []);

  if (loading) {
    return <Spinner />;
  }
  return (
    <>
      <CRow>
        <CCol xl="12" lg="6">
          <CCard>
            <CCardHeader>
              <CCol col="6" sm="4" md="2" xl>
                <CButton
                  onClick={openModal}
                  style={{ float: "right" }}
                  active
                  color="success"
                  aria-pressed="true"
                >
                  Add Staff
                </CButton>
                Staffs List
              </CCol>
            </CCardHeader>
            <CCardBody>
              <StaffTable
                userList={userList}
                setUserDelete={setUserDelete}
                setUserEdit={setUserEdit}
                setUserModal={setUserModal}
                setShowbtn={setShowbtn}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <StaffForm
        showUserModal={showUserModal || !!userEdit}
        closeModal={closeModal}
        userEdit={userEdit}
        showbtn={showbtn}
      />
      <DeleteStaff closeModal={closeDeleteUser} showModal={userDelete} />
    </>
  );
};

export default StaffScreen;
