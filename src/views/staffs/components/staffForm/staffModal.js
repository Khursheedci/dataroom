import React from "react";
import { CModal, CModalBody, CModalTitle, CModalHeader } from "@coreui/react";

import UserForm from "./StaffsForm";

const UserModal = ({ showUserModal, closeModal, userEdit, showbtn }) => (
  <CModal show={showUserModal} onClose={() => closeModal(false)} size="lg">
    <CModalHeader closeButton>
      <CModalTitle>
        {showbtn ? "Staff Profile" : "Staff Registration Form"}
      </CModalTitle>
    </CModalHeader>
    <CModalBody>
      {showUserModal && (
        <UserForm
          closeModal={closeModal}
          userEdit={userEdit}
          showbtn={showbtn}
        />
      )}
    </CModalBody>
  </CModal>
);

export default UserModal;
