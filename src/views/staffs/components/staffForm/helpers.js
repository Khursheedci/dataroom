/**
 * Transform Data for API Specific format
 * @param {*} data
 * @returns
 */
export const transformForRequest = ({ phoneCode, ...rest }) => {
  const transformmedData = {
    ...rest,
    phoneNumber: `${phoneCode}${rest.phoneNumber}`,
  };
  return transformmedData;
};

/**
 * Transform Data from API into form data
 * @param {*} data
 * @returns
 */
export const transformFromRequest = ({
  firstName,
  lastName,
  staffId,
  email,
  department,
  phoneNumber,
}) => {
  const transformmedData = {
    firstName,
    lastName,
    email,
    staffId,
    department,
    phoneNumber: phoneNumber.split("").slice(5).join(""),
    phoneCode: phoneNumber.split("").slice(0, 5).join(""),
  };
  console.log(":::>>>>>>>>>>>", transformmedData);
  return transformmedData;
};
