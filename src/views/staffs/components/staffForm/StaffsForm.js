import React, { useContext, useEffect, useState } from "react";
import UserServices from "../../../../services/staffs";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow,
  CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { DocsLink } from "src/reusable";
import {
  initialForm,
  roomPrivOptions,
  editInitialForm,
  otherPrivOptions,
} from "./constants";
import { transformForRequest, transformFromRequest } from "./helpers";
import { validationSchema } from "./formValidation";
import { formValidator } from "../../../../libs";
import { ToastContext } from "../../../../context";
import _ from "lodash";

const StaffForm = ({ closeModal, userEdit, showUserModal, showbtn }) => {
  const userService = new UserServices();

  console.log("shobtn in basic form is >>>>>", showbtn);
  // const [collapsed, setCollapsed] = React.useState(true);
  // const [showElements, setShowElements] = React.useState(true);
  const [formValues, setFormValues] = useState(
    !userEdit
      ? { ...initialForm, roomPrivlege: [] }
      : transformFromRequest(userEdit)
  );
  const [selected, setSelected] = useState(
    !userEdit ? [] : []
    // : roomPrivOptions.filter((priv) =>
    //     userEdit.roomPrivlege.includes(priv.value)
    //   )
  );
  const [formErrors, setFormErrors] = useState(initialForm);
  const [formTouched, setFormTouched] = useState(
    !userEdit ? initialForm : editInitialForm
  );
  const [isLoading, setLoading] = useState(false);
  const isFormValid = () =>
    Object.values(formValues).every((val) => val !== undefined) &&
    Object.values(formTouched).every((val) => val === true) &&
    Object.values(formErrors).length === 0;
    const { addToast } = useContext(ToastContext);

  const validateForm = () => {
    const errors = formValidator(validationSchema, formValues, formTouched);
    setFormErrors(errors);
  };
  const setValue = (key, value) => {
    setFormValues({ ...formValues, [key]: value });
  };
  const handleTouch = (key) => setFormTouched({ ...formTouched, [key]: true });

  const onFormSubmit = (event) => {
    console.log("submitting..", "data>>>", formValues);
    event.preventDefault();
    setLoading(true);
    const body = transformForRequest(formValues);
    console.log("submitting..", "body>>>", body);
    if (!userEdit) {
      userService
        .createUser(body)
        .then((res) => closeModal(true))
        .catch((err) => console.log("erss", err))
        .finally(() => setLoading(false));
    } else {
      userService
        .updateUser(userEdit.originalId, body)
        .then((res) => closeModal(true))
        .catch(() => addToast("error"))
        .finally(() => setLoading(false));
    }
  };

  useEffect(validateForm, [formValues, selected]);

  useEffect(() => {
    setValue(
      "roomPrivlege",
      selected.map((val) => val.value)
    );
    setFormTouched({ ...formTouched, roomPrivlege: true });
  }, [selected]);

  const hasDifference = () => {
    return !userEdit
      ? false
      : _.isEqual(formValues, transformFromRequest(userEdit));
  };
  const phoneCodes = [
    23225, 23230, 23231, 23232, 23233, 23234, 23244, 23275, 23276, 23277, 23278,
    23279, 23280, 23288, 23299,
  ];
  const setPhoneNumber = (value) => {
    if (/^[0-9]{0,6}$/.test(value) && value <= 999999) {
      setValue("phoneNumber", value);
    }
  };
  return (
    <>
      <CRow>
        <CCol xl="12" md="6">
          <CCard>
            <CCardBody>
              <CForm onSubmit={onFormSubmit} className="form-horizontal">
                <CFormGroup row>
                  <CCol xs="12" md="9" lg="6">
                    <CCol xs="12" md="9" lg="12">
                      <CLabel htmlFor="first-name">First Name</CLabel>
                    </CCol>
                    <CCol xs="12" md="9" lg="12">
                      <CInput
                        disabled={showbtn}
                        id="first-name"
                        name="first-name"
                        placeholder="Enter First Name"
                        value={formValues.firstName}
                        onChange={(e) => setValue("firstName", e.target.value)}
                        onFocus={() => handleTouch("firstName")}
                        className="bg-white text-dark"
                        style = {{border:formErrors.hasOwnProperty('firstName')  ? '2px solid red' : '' }}
                      />
                        <span className ='text-danger' >
                          {formErrors.firstName}
                        </span>
                    </CCol>
                  </CCol>
                  <CCol xs="12" md="9" lg="6">
                    <CCol xs="12" md="9" lg="12">
                      <CLabel htmlFor="text-input">Last Name</CLabel>
                    </CCol>
                    <CCol xs="12" md="9" lg="12">
                      <CInput
                        disabled={showbtn}
                        id="last-name"
                        name="last-name"
                        placeholder="Enter Last Name"
                        value={formValues.lastName}
                        onChange={(e) => setValue("lastName", e.target.value)}
                        onFocus={() => handleTouch("lastName")}
                        className="bg-white text-dark"
                        style = {{border:formErrors.hasOwnProperty('lastName')  ? '2px solid red' : '' }}
                      />
                        <span className ='text-danger' >
                        {formErrors.lastName}
                        </span>
                    </CCol>
                  </CCol>
                </CFormGroup>

                <CFormGroup>
                  <CRow>
                    <CCol xs="12" md="9" lg="6">
                      <CCol xs="12" md="9" lg="12">
                        <CLabel htmlFor="mobileNo-input"> Mobile Code </CLabel>
                      </CCol>
                      <CCol xs="12" md="9" lg="12">
                        <CSelect
                          disabled={showbtn}
                          custom
                          name="mobile-code"
                          id="mobile-code"
                          className="bg-white text-dark"
                          onChange={(e) =>
                            setValue("phoneCode", Number(e.target.value))
                          }
                          onFocus={() => handleTouch("phoneCode")}
                        >
                          <option
                            value={0}
                            selected={formValues.phoneCode === undefined}
                            disabled
                          >
                            {" "}
                            Please select{" "}
                          </option>
                          {phoneCodes.map((val) => (
                            <option
                              selected={formValues.phoneCode == val}
                              key={val}
                              value={val}
                            >
                              {val}
                            </option>
                          ))}
                        </CSelect>
                      </CCol>
                    </CCol>
                    <CCol xs="12" md="9" lg="6">
                      <CCol xs="12" md="9" lg="12">
                        <CLabel htmlFor="phone-number"> Phone No </CLabel>
                      </CCol>
                      <CCol xs="12" md="9" lg="12">
                        <CInput
                          disabled={showbtn}
                          id="phone-number"
                          name="phone-number"
                          placeholder="Enter Phone No"
                          className="bg-white text-dark"
                          value={formValues.phoneNumber}
                          minLength={6}
                          maxLength={6}
                          onChange={(e) => setPhoneNumber(e.target.value)}
                          onFocus={() => handleTouch("phoneNumber")}
                          style = {{border:formErrors.hasOwnProperty('phoneNumber')  ? '2px solid red' : '' }}
                        />
                           <span className ='text-danger' >
            {formErrors.phoneNumber}
                </span>
                      </CCol>
                    </CCol>
                  </CRow>
                </CFormGroup>
                <CFormGroup>
                  <CRow>
                    <CCol xs="12" md="9" lg="6">
                      <CCol xs="12" md="9" lg="12">
                        <CLabel htmlFor="email-input">Email Input</CLabel>
                      </CCol>
                      <CCol xs="12" md="9" lg="12">
                        <CInput
                          disabled={showbtn}
                          type="email"
                          id="email-input"
                          name="email-input"
                          placeholder="Enter Email"
                          autoComplete="email"
                          value={formValues.email}
                          onChange={(e) => setValue("email", e.target.value)}
                          onFocus={() => handleTouch("email")}
                          className="bg-white text-dark"
                          style = {{border:formErrors.hasOwnProperty('email')  ? '2px solid red' : '' }}
                        />
                           <span className ='text-danger' >
            {formErrors.email}
                </span>
                      </CCol>
                    </CCol>
                    <CCol xs="12" md="9" lg="6">
                      <CCol xs="12" md="9" lg="12">
                        <CLabel htmlFor="select"> Department </CLabel>
                      </CCol>
                      <CCol xs="12" md="9" lg="12">
                        <CSelect
                          disabled={showbtn}
                          custom
                          name="department"
                          id="department"
                          className="bg-white text-dark"
                          onChange={(e) =>
                            setValue("department", Number(e.target.value))
                          }
                          onFocus={() => handleTouch("department")}
                        >
                          {otherPrivOptions.map((val) => (
                            <option
                              selected={formValues.department == val.value}
                              key={val.value}
                              value={val.value}
                            >
                              {val.name}
                            </option>
                          ))}
                        </CSelect>
                      </CCol>
                    </CCol>
                  </CRow>
                </CFormGroup>
                <CFormGroup>
                  <CRow>
                    <CCol xs="12" md="9" lg="6">
                      <CCol xs="12" md="9" lg="12">
                        <CLabel htmlFor="text-input">Staff ID No.</CLabel>
                      </CCol>
                      <CCol xs="12" md="9" lg="12">
                        <CInput
                          id="staff-id"
                          name="staff-id"
                          placeholder="Enter staff ID"
                          value={formValues.staffId}
                          onChange={(e) => setValue("staffId", e.target.value)}
                          onFocus={() => handleTouch("staffId")}
                          disabled={showbtn}
                          className="bg-white text-dark"
                          style = {{border:formErrors.hasOwnProperty('staffId')  ? '2px solid red' : '' }}
                        />
                           <span className ='text-danger' >
            {formErrors.staffId}
                </span>
                      </CCol>
                    </CCol>
                  </CRow>
                </CFormGroup>               
                {!userEdit && !showbtn && (
                  <CButton
                    className="submit_btn"
                    type="submit"
                    disabled={!isFormValid() || isLoading}
                    size="xs"
                    color="primary"
                  >
                    <CIcon name="cil-scrubber" /> Create staff
                  </CButton>
                )}

                {!!userEdit && !showbtn && (
                  <CButton
                    className="submit_btn"
                    type="submit"
                    disabled={!isFormValid() && !hasDifference() || isLoading}
                    size="xs"
                    color="primary"
                  >
                    <CIcon name="cil-scrubber" /> Update staff: {`${!isFormValid()} ${hasDifference()}`}
                  </CButton>
                )}
              </CForm>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default StaffForm;
