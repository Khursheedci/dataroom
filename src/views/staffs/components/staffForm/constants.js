export const initialForm = {
  firstName: undefined,
  lastName: undefined,
  staffId: undefined,
  email: undefined,
  department: undefined,
  phoneNumber: undefined,
};

export const editInitialForm = {
  firstName: true,
  lastName: true,
  staffId: true,
  email: true,
  department: true,
  phoneNumber: true,
};

export const roomPrivOptions = [
  { label: "Please select", value: 0 },
  { label: "Sales", value: 1 },
  { label: "Finance", value: 2 },
  { label: "Operations", value: 3 },
  { label: "Logistics", value: 4 },
  { label: "MD's Office", value: 5 },
  { label: "Network Management", value: 6 },
  { label: "IT", value: 7 },
];
export const otherPrivOptions = [
  { name: "Please select", value: 0 },
  { name: "Sales", value: 1 },
  { name: "Finance", value: 2 },
  { name: "Operations", value: 3 },
  { name: "Logistics", value: 4 },
  { name: "MD's Office", value: 5 },
  { name: "Network Management", value: 6 },
  { name: "IT", value: 7 },
];
