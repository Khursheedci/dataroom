import React, { useState } from "react";
import {
  CDataTable,
  CCollapse,
  CCardBody,
  CRow,
  CCol,
  CCallout,
  CBadge,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import NoItemView from "../../noItemView/noItemView";

const fields = [
  { key: "fullName", label: "Name" },
  { key: "email", label: "Email" },
  { key: "staffId", label: "Staff ID" },
  { key: "Actions", label: "Actions" },
];
const roomPrivOptions = [{ label: "Department", value: 1 }];
const otherPriv = {
  1: "Draft",
  2: "Final",
};

const UserTable = ({
  userList,
  setUserEdit,
  setUserDelete,
  setUserModal,
  setShowbtn,
}) => {
  const [details, setDetails] = useState([]);

  // const toggleDetails = ({ originalId: id }) => {
  //   const position = details.indexOf(id);
  //   let newDetails = details.slice();
  //   if (position !== -1) {
  //     newDetails.splice(position, 1);
  //   } else {
  //     newDetails.push(id);
  //   }
  //   setDetails(newDetails);
  // };
  const showProfile = (item) => {
    setShowbtn(true);
    setUserEdit(item);
  };
  const showEdit = (item) => {
    setShowbtn(false);
    setUserEdit(item);
  };

  userList.forEach((element) => {
    element.fullName = element.firstName + " " + element.lastName;
  });

  const actions = [
    { icon: freeSet.cilUser, onClick: showProfile },
    { icon: freeSet.cilPenAlt, onClick: showEdit },
    { icon: freeSet.cilUserX, onClick: setUserDelete },
  ];

  return (
    <CDataTable
      items={userList}
      fields={fields}
      bordered
      itemsPerPage={5}
      pagination
      tableFilter={{ placeholder: "search..." }}
      noItemsViewSlot={<NoItemView />}
      scopedSlots={{
        Actions: (item) => (
          <div>
            {actions.map((action) => (
              <td>
                <CIcon
                  size={"lg"}
                  content={action.icon}
                  onClick={() => {
                    console.log("check>>>>>>>>>>", item);
                    action.onClick(item);
                  }}
                />
              </td>
            ))}
          </div>
        ),
        // details: (item, index) => (
        //   <CCollapse show={details.includes(item.originalId)}>
        //     <CCardBody>
        //       <CRow>
        //         {/* <CCol col="12" sm="4">
        //           <CCallout color="info" className={"bg-secondary"}>
        //             <small className="text-muted">Department</small>
        //             <br />
        //             <strong className="h6">
        //               {roomPrivOptions
        //                 .filter((priv) =>
        //                   item.roomPrivlege.includes(priv.value)
        //                 )
        //                 .map((priv) => priv.label)
        //                 .join(" ,")}
        //             </strong>
        //           </CCallout>
        //         </CCol> */}
        //         {/* <CCol col="12" sm="4">
        //           <CCallout color="info" className={"bg-secondary"}>
        //             <small className="text-muted">Report Privleges</small>
        //             <br />
        //             <strong className="h6">
        //               {otherPriv[item.reportPrivlege]}
        //             </strong>
        //           </CCallout>
        //         </CCol> */}
        //         {/* <CCol col="12" sm="4">
        //           <CCallout color="info" className={"bg-secondary"}>
        //             <small className="text-muted">KPI Privleges</small>
        //             <br />
        //             <strong className="h6">
        //               {otherPriv[item.KPIPrivlege]}
        //             </strong>
        //           </CCallout>
        //         </CCol> */}
        //       </CRow>
        //       <h4>{item.username}</h4>
        //       <p className="text-muted">User since: {item.createdAt}</p>
        //     </CCardBody>
        //   </CCollapse>
        // ),
      }}
    />
  );
};

export default UserTable;
