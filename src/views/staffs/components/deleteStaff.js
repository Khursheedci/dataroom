import React, { useState } from "react";
import {
  CModal,
  CModalBody,
  CModalHeader,
  CModalFooter,
  CButton,
} from "@coreui/react";
import UserServices from "../../../services/staffs";

const userService = new UserServices();

const DeletUser = ({ showModal, closeModal }) => {
  const [loading, setLoading] = useState(false);

  const onClickDelete = () => {
    userService.deleteUser(showModal.originalId).then(() => {
      setLoading(false);
      closeModal(true);
    });
  };
  return (
    <>
      <CModal show={!!showModal} onClose={() => closeModal(false)}>
        {!!showModal && (
          <>
            <CModalHeader closeButton>Modal title</CModalHeader>
            <CModalBody>
              Do you really want to delete user for {showModal.firstName} ?
            </CModalBody>
            <CModalFooter>
              <CButton
                color="primary"
                disabled={loading}
                onClick={onClickDelete}
              >
                Delete
              </CButton>
              <CButton
                color="secondary"
                onClick={() => closeModal(false)}
                disabled={loading}
              >
                Cancel
              </CButton>
            </CModalFooter>
          </>
        )}
      </CModal>
    </>
  );
};

export default DeletUser;
