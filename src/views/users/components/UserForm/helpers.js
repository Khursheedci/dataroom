/**
 * Transform Data for API Specific format
 * @param {*} data
 * @returns
 */
export const transformForRequest = ({ phoneCode, ...rest }) => {
  const transformmedData = {
    ...rest,
    phoneNumber: `${phoneCode}${rest.phoneNumber}`,
  };
  return transformmedData;
};

/**
 * Transform Data from API into form data
 * @param {*} data
 * @returns
 */
export const transformFromRequest = ({
  phoneNumber,
  firstName,
  lastName,
  email,
 // dataRoomPrivilegeId,
  dataRoomId,
  reportPrivlege,
  KPIPrivlege,
   password,
   confirmPassword
}) => {
  const transformmedData = {
    firstName,
    lastName,
    email,
    // dataRoomPrivilegeId,
    dataRoomId,
    reportPrivlege,
    KPIPrivlege,
    password,
    confirmPassword,
    phoneNumber: phoneNumber.split("").slice(5).join(""),
    phoneCode: phoneNumber.split("").slice(0, 5).join(""),
  };  
  return transformmedData;
};
