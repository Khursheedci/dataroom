export const initialForm = {
  firstName: undefined,
  lastName: undefined,
  phoneCode: undefined,
  phoneNumber: undefined,
  email: undefined,
 // dataRoomPrivilegeId: undefined,
  dataRoomId: undefined,
  reportPrivlege: undefined,
  KPIPrivlege: undefined,
  password: undefined,
  confirmPassword:undefined
};

export const editInitialForm = {
  firstName: true,
  lastName: true,
  phoneCode: true,
  phoneNumber: true,
  email: true,
  // dataRoomPrivilegeId: true,
  dataRoomId: true,
  reportPrivlege: true,
  KPIPrivlege: true,
  password: true,
  confirmPassword:true
};

export const phoneCodes = [
  23225, 23230, 23231, 23232, 23233, 23234, 23244, 23275, 23276, 23277, 23278,
  23279, 23280, 23288, 23299,
];
export const roomPrivOptions = [
  { label: "Board", value: "1" },
  { label: "Senior Management Cabinet", value: "2" },
  { label: "Departmental Cabinet", value: "3" },
];
export const otherPrivOptions = [
  { name: "Draft", value: 1 },
  { name: "Final", value: 2 },
];
