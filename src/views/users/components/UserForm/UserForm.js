import React, { useContext, useEffect, useState } from "react";
import {
  CButton,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  CRow,
  CValidFeedback,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import MultiSelect from "react-multi-select-component";
import _ from "lodash";

import {
  initialForm,
  phoneCodes,
  roomPrivOptions,
  otherPrivOptions,
  editInitialForm,
} from "./constants";
import { transformForRequest, transformFromRequest } from "./helpers";
import { UserServices } from "../../../../services";
import { validationSchema } from "./formValidation";
import { formValidator } from "../../../../libs";
import { ToastContext } from "../../../../context";

const userService = new UserServices();

const BasicForms = ({ closeModal, userEdit, showbtn }) => {  
  const [formValues, setFormValues] = useState(
    !userEdit
      ? { ...initialForm, dataRoomId: [] }
      : transformFromRequest(userEdit)
  );
  const [selected, setSelected] = useState(
    !userEdit
      ? []
      : roomPrivOptions.filter((priv) =>
          userEdit.dataRoomId.includes(priv.value)
        )
  );
  const [formErrors, setFormErrors] = useState(initialForm);
  const [formTouched, setFormTouched] = useState(
    !userEdit ? initialForm : editInitialForm
  );
  const [isLoading, setLoading] = useState(false);
  const isFormValid = () =>
    Object.values(formValues).every((val) => val !== undefined) &&
    Object.values(formTouched).every((val) => val === true) &&
    Object.values(formErrors).length === 0 && 
    formValues.dataRoomId.length > 0

    const { addToast } = useContext(ToastContext);

  const validateForm = () => {
    const errors = formValidator(validationSchema, formValues, formTouched);    
    setFormErrors(errors);
  };
  const setValue = (key, value) =>
    setFormValues({ ...formValues, [key]: value });
  const handleTouch = (key) => setFormTouched({ ...formTouched, [key]: true });

  const onFormSubmit = (event) => {    
      formValues.dataRoomPrivilegeId = [];      
      event.preventDefault();
      setLoading(true);
      const body = transformForRequest(formValues);
      if (!userEdit) {
        userService
          .createUser(body)
          .then((res) => closeModal(true))
          .catch((err) => {
            console.log(err.response.data.error[0].msg);
            addToast(err.response.data.error[0].msg);
          })
          .finally(() => setLoading(false));
      } else {
        userService
          .updateUser(userEdit.originalId, body)
          .then((res) => closeModal(true))
          .catch((error) => console.log(error.response.status))
          .finally(() => setLoading(false));
      }    
  };

  const setPhoneNumber = (value) => {
    if (/^[0-9]{0,6}$/.test(value) && value <= 999999) {
      setValue("phoneNumber", value);
    }
  };

  useEffect(validateForm, [formValues, selected]);

  useEffect(() => {
    setValue(
      "dataRoomId",
      selected.map((val) => val.value)
    );
    setFormTouched({ ...formTouched, dataRoomId: true });
  }, [selected]);

  const hasDifference = () => {
    return !userEdit
      ? false
      : _.isEqual(formValues, transformFromRequest(userEdit));
  };

  useEffect(() => {
    if (showbtn) {
      let selecteForShow = selected;
      selecteForShow.forEach((e) => {
        e.disabled = true;
      });
    } else {
      let selecteForShow = selected;
      selecteForShow.forEach((e) => {
        e.disabled = false;
      });
    }
  }, []);

  return (
    <>
      <CRow>
        <CCol lg="12" xs="12">
          <CForm onSubmit={onFormSubmit} className="form-horizontal">
            <CFormGroup row>
              <CCol xs="12" md="9" lg="6">
                <CCol xs="12" md="9" lg="12">
                  <CLabel htmlFor="first-name"> First Name </CLabel>
                </CCol>
                <CCol xs="12" md="9" lg="12">
                  <CInput
                    disabled={showbtn}
                    id="first-name"
                    name="first-name"
                    placeholder="Enter First Name"
                    value={formValues.firstName}
                    onChange={(e) => setValue("firstName", e.target.value)}
                    onFocus={() => handleTouch("firstName")}
                    className="bg-white text-dark"
                    style = {{border:formErrors.hasOwnProperty('firstName')  ? '2px solid red' : '' }}
                    //invalid
                  />
                   <span className ='text-danger' >
            {formErrors.firstName}
                </span>
                </CCol>
              </CCol>
              <CCol xs="12" md="9" lg="6">
                <CCol xs="12" md="9" lg="12">
                  <CLabel htmlFor="last-name"> Last Name </CLabel>
                </CCol>
                <CCol xs="12" md="9" lg="12">
                  <CInput
                    disabled={showbtn}
                    id="last-name"
                    name="last-name"
                    placeholder="Enter Last Name"
                    value={formValues.lastName}
                    onChange={(e) => setValue("lastName", e.target.value)}
                    onFocus={() => handleTouch("lastName")}
                    className="bg-white text-dark"
                    style = {{border:formErrors.hasOwnProperty('lastName')  ? '2px solid red' : '' }}
                  />
                   <span className ='text-danger' >
            {formErrors.lastName}
                </span>
                </CCol>
              </CCol>
            </CFormGroup>
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="text-input"> Mobile Code </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      disabled={showbtn}
                      custom
                      name="mobile-code"
                      id="mobile-code"
                      className="bg-white text-dark"
                      onChange={(e) =>
                        setValue("phoneCode", Number(e.target.value))
                      }
                      onFocus={() => handleTouch("phoneCode")}
                    >
                      <option
                        value={0}
                        selected={formValues.phoneCode === undefined}
                        disabled
                      >
                        {" "}
                        Please select{" "}
                      </option>
                      {phoneCodes.map((val) => (
                        <option
                          selected={formValues.phoneCode == val}
                          key={val}
                          value={val}
                        >
                          {val}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="phone-number"> Phone No </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      disabled={showbtn}
                      id="phone-number"
                      name="phone-number"
                      placeholder="Enter Phone No"
                      className="bg-white text-dark"
                      value={formValues.phoneNumber}
                      minLength={6}
                      maxLength={6}
                      onChange={(e) => setPhoneNumber(e.target.value)}
                      onFocus={() => handleTouch("phoneNumber")}
                      style = {{border:formErrors.hasOwnProperty('phoneNumber')  ? '2px solid red' : '' }}
                    />
                     <span className ='text-danger' >
            {formErrors.phoneNumber}
                </span>
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>

            
              <CFormGroup row>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="first-name"> Create Password </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      disabled={showbtn}
                      id="password"
                      name="password"
                      placeholder="Enter Password"
                      className="bg-white text-dark"
                      value={formValues.password}
                      onChange={(e) => setValue("password", e.target.value)}
                      onFocus={() => handleTouch("password")}
                      style = {{border:formErrors.hasOwnProperty('password')  ? '2px solid red' : '' }}
                      //invalid
                    />
                     <span className ='text-danger' >
            {formErrors.password}
                </span>
                  </CCol>
                </CCol>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="confirm-password"> Confirm Password </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      disabled={showbtn}
                      id="confirm-password"
                      name="confirm-password"
                      placeholder="Confirm Password"
                      className="bg-white text-dark"
                      value={formValues.confirmPassword}
                      onChange={(e) =>
                        setValue("confirmPassword", e.target.value)
                      }
                      onFocus={() => handleTouch("confirmPassword")}
                      style = {{border:formErrors.hasOwnProperty('confirmPassword')  ? '2px solid red' : '' }}
                    />
                     <span className ='text-danger' >
            {formErrors.confirmPassword}
                </span>
                  </CCol>
                </CCol>
              </CFormGroup>
          
            <CFormGroup row>
              <CCol xs="12" md="9" lg="6">
                <CCol xs="12" md="9" lg="12">
                  <CLabel htmlFor="email-input"> Email Input </CLabel>
                </CCol>
                <CCol xs="12" md="9" lg="12">
                  <CInput
                    disabled={showbtn}
                    type="email"
                    id="email-input"
                    name="email-input"
                    placeholder="Enter Email"
                    autoComplete="email"
                    value={formValues.email}
                    onChange={(e) => setValue("email", e.target.value)}
                    onFocus={() => handleTouch("email")}
                    className="bg-white text-dark"
                  />
                   <span className ='text-danger' >
            {formErrors.email}
                </span>
                </CCol>
              </CCol>
              <CCol xs="12" md="9" lg="6">
                <CCol xs="12" md="9" lg="12">
                  <CLabel htmlFor="dataRoomId"> Data Room Privilege </CLabel>
                </CCol>
                <CCol xs="12" md="9" lg="12">
                  <MultiSelect
                    options={showbtn ? selected : roomPrivOptions}
                    value={selected}
                    onChange={setSelected}
                    labelledBy="Select"
                    disableSearch={true}
                    hasSelectAll={showbtn}
                    className="bg-white text-dark"
                  />
                </CCol>
              </CCol>
            </CFormGroup>
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="reportPrivlege">
                      Report Submission Privilege
                    </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      disabled={showbtn}
                      custom
                      name="reportPrivlege"
                      id="reportPrivlege"
                      className="bg-white text-dark"
                      onChange={(e) =>
                        setValue("reportPrivlege", Number(e.target.value))
                      }
                      onFocus={() => handleTouch("reportPrivlege")}
                    >
                      <option
                        value={0}
                        selected={formValues.reportPrivlege == undefined}
                        disabled
                      >
                        {" "}
                        Please select{" "}
                      </option>
                      {otherPrivOptions.map((val) => (
                        <option
                          selected={formValues.reportPrivlege === val.value}
                          key={val.value}
                          value={val.value}
                        >
                          {val.name}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="KPIPrivlege">
                      KPI Management Privilege
                    </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      disabled={showbtn}
                      custom
                      name="KPIPrivlege"
                      id="KPIPrivlege"
                      className="bg-white text-dark"
                      onChange={(e) =>
                        setValue("KPIPrivlege", Number(e.target.value))
                      }
                      onFocus={() => handleTouch("KPIPrivlege")}
                    >
                      <option
                        value={0}
                        selected={formValues.KPIPrivlege == undefined}
                        disabled
                      >
                        {" "}
                        Please select{" "}
                      </option>
                      {otherPrivOptions.map((val) => (
                        <option
                          selected={formValues.KPIPrivlege === val.value}
                          key={val.value}
                          value={val.value}
                        >
                          {val.name}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>            
            {!userEdit && !showbtn && (
              <CButton
                className="submit_btn"
                type="submit"
                disabled={!isFormValid() || isLoading}
                size="xs"
                color="primary"
              >
                <CIcon name="cil-scrubber" /> Create User 
              </CButton>
            )}
            {!!userEdit && !showbtn && (
              <CButton
                className="submit_btn"
                type="submit"
                disabled={!isFormValid() || hasDifference() || isLoading}
                size="xs"
                color="primary"
              >
                <CIcon name="cil-scrubber" /> Update User
              </CButton>
            )}
          </CForm>
        </CCol>
      </CRow>
    </>
  );
};

export default BasicForms;
