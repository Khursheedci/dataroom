import React, { useState } from "react";
import {
  CModal,
  CModalBody,
  CModalHeader,
  CModalFooter,
  CButton,
} from "@coreui/react";
import UserServices from "../../../../services/report";

const userService = new UserServices();

const DeletUser = ({ showModal, closeModal }) => {
  const [loading, setLoading] = useState(false);
  const onClickDelete = () => {
    console.log(showModal.originalId);
    userService.deleteUser(showModal.originalId).then(() => {
      setLoading(false);
      closeModal(true);
    });
  };
  return (
    <>
      <CModal show={!!showModal} onClose={() => closeModal(false)}>
        {!!showModal && (
          <>
            <CModalHeader closeButton>Delete Report Type</CModalHeader>
            <CModalBody>
              Do you really want to delete {showModal.reportName} ?
            </CModalBody>
            <CModalFooter>
              <CButton
                color="primary"
                disabled={loading}
                onClick={onClickDelete}
              >
                Delete
              </CButton>
              <CButton
                color="secondary"
                onClick={() => closeModal(false)}
                disabled={loading}
              >
                Cancel
              </CButton>
            </CModalFooter>
          </>
        )}
      </CModal>
    </>
  );
};

export default DeletUser;
