import React, { useContext, useEffect, useState } from "react";
import {
  CButton,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  CRow,
  CTextarea,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import MultiSelect from "react-multi-select-component";
import _ from "lodash";

import {
  initialForm,
  phoneCodes,
  statusOptions,
  frequencyOptions,
  otherPrivOptions,
  editInitialForm,
} from "./constants";
import { transformForRequest, transformFromRequest } from "./helpers";
import UserServices from "../../../../services/report";
//import { validationSchema } from "./formValidation";
import { formValidator } from "../../../../libs";
import { ToastContext } from "../../../../context";
import StaffServices from "../../../../services/staffs";
import TeamServices from "../../../../services/teams";
import { element } from "prop-types";

const userService = new UserServices();
const staffService = new StaffServices();
const teamServices = new TeamServices();

const BasicForms = ({ closeModal, userEdit, showbtn }) => {
  const [calendarValue, setCalendarValue] = useState(new Date());
  const [calenderOn, setCalenderOn] = useState(false);
  const [staffList, setStaffList] = useState([]);
  const [teamList, setTeamList] = useState([]);
  const [selectedMembers, setSelecetedMembers] = useState([]);

  const [formValues, setFormValues] = useState(
    !userEdit ? { ...initialForm } : transformFromRequest(userEdit)
  );
  const [reportTypeBy, setReportTypeBy] = useState(
    userEdit ? userEdit.reportTypeBy : 1
  );
  const [selected, setSelected] = useState(
    !userEdit ? [] : []
    // roomPrivOptions.filter((priv) =>
    //     userEdit.addMember.includes(priv.value)
    //   )
  );

  const [formErrors, setFormErrors] = useState(initialForm);
  const [formTouched, setFormTouched] = useState(
    !userEdit ? initialForm : editInitialForm
  );
  const [isLoading, setLoading] = useState(false);
  const isFormValid = () =>
    Object.values(formValues).every((val) => val !== undefined) &&
    Object.values(formTouched).every((val) => val === true) &&
    Object.values(formErrors).length === 0;
  const { addToast } = useContext(ToastContext);

  // const validateForm = () => {
  //   const errors = formValidator(validationSchema, formValues, formTouched);
  //   setFormErrors(errors);
  // };

  const setValue = (key, value) =>
    setFormValues({ ...formValues, [key]: value });
  const handleTouch = (key) => setFormTouched({ ...formTouched, [key]: true });

  const onFormSubmit = (event) => {
    formValues.reportTypeId = selectedMembers.map((e) => e.value);
    console.log("formvalues are.......", formValues);
    event.preventDefault();
    setLoading(true);
    const body = transformForRequest(formValues);
    if (!userEdit) {
      userService
        .createUser(body)
        .then((res) => closeModal(true))
        .catch((err) => {
          alert(err);
          console.log("erss", err);
        })
        .finally(() => setLoading(false));
    } else {
      userService
        .updateUser(userEdit.originalId, body)
        .then((res) => closeModal(true))
        .catch(() => addToast("error"))
        .finally(() => setLoading(false));
    }
  };

  // useEffect(validateForm, [formValues, selected]);

  // useEffect(() => {
  //   setValue(
  //     "type",
  //     selected.map((val) => val.value)
  //   );
  //   setFormTouched({ ...formTouched, roomPrivlege: true });
  // }, [selected]);

  const hasDifference = () => {
    return !userEdit
      ? false
      : _.isEqual(formValues, transformFromRequest(userEdit));
  };

  let options = [];
  let selectedOptions = [];

  const UpadateList = () => {
    if (userEdit ? formValues.reportTypeBy == 1 : reportTypeBy == 1) {
      staffService
        .getUsers()
        .then((res) => {
          res.forEach((element) => {
            options.push({ label: element.firstName, value: element.id });
            userEdit &&
              formValues.reportTypeId.includes(element.id) &&
              selectedOptions.push({
                label: element.firstName,
                value: element.id,
                disabled: true,
              });
          });
          setStaffList(options);
          setSelecetedMembers(selectedOptions);
        })
        .catch((err) => console.log(":::", err));
    }
    if (userEdit ? formValues.reportTypeBy == 2 : reportTypeBy == 2) {
      teamServices
        .getUsers()
        .then((res) => {
          res.forEach((element) => {
            options.push({ label: element.teamName, value: element.id });
            userEdit &&
              formValues.reportTypeId.includes(element.id) &&
              selectedOptions.push({
                label: element.teamName,
                value: element.id,
                disabled: true,
              });
          });
          setStaffList(options);
          setSelecetedMembers(selectedOptions);
        })
        .catch((err) => console.log(":::", err));
    }

    console.log("options are>>>>>>>>", options);
    console.log("selectedoptins are>>>>>>>>", selectedOptions);
  };

  useEffect(UpadateList, [reportTypeBy]);

  return (
    <>
      <CRow>
        <CCol lg="12" xs="12">
          <CForm onSubmit={onFormSubmit} className="form-horizontal">
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="first-name"> Report Name </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      disabled={showbtn}
                      id="Report-name"
                      name="Report-name"
                      placeholder="Enter Report Name"
                      value={formValues.reportName}
                      onChange={(e) => setValue("reportName", e.target.value)}
                      onFocus={() => handleTouch("reportName")}
                    />
                  </CCol>
                </CCol>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="last-name"> Description </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CTextarea
                      disabled={showbtn}
                      rows="1"
                      id="last-name"
                      name="last-name"
                      placeholder="Enter Description"
                      value={formValues.description}
                      onChange={(e) => setValue("description", e.target.value)}
                      onFocus={() => handleTouch("description")}
                    />
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="last-name"> Start Date </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      disabled={showbtn}
                      name="reportStartDate"
                      id="reportStartDate"
                      type="date"
                      value={formValues.reportStartDate}
                      onChange={(e) => {
                        setValue("reportStartDate", e.target.value);
                        setCalendarValue(e.target.value);
                      }}
                    ></CInput>
                  </CCol>
                </CCol>

                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="Members"> Status </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      disabled={showbtn}
                      custom
                      name="Status"
                      id="Status"
                      onChange={(e) =>
                        setValue("status", Number(e.target.value))
                      }
                      onFocus={() => handleTouch("status")}
                    >
                      <option
                        value={0}
                        selected={formValues.status == undefined}
                        disabled
                      >
                        {" "}
                        Please select{" "}
                      </option>
                      {statusOptions.map((val) => (
                        <option
                          selected={formValues.status === val.value}
                          key={val.value}
                          value={val.value}
                        >
                          {val.name}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="Members"> Frequency </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      disabled={showbtn}
                      custom
                      name="Frequency"
                      id="Frequency"
                      onChange={(e) =>
                        setValue("frequency", Number(e.target.value))
                      }
                      onFocus={() => handleTouch("frequency")}
                    >
                      <option
                        value={0}
                        selected={formValues.frequency == undefined}
                        disabled
                      >
                        {" "}
                        Please select{" "}
                      </option>
                      {frequencyOptions.map((val) => (
                        <option
                          selected={formValues.frequency === val.value}
                          key={val.value}
                          value={val.value}
                        >
                          {val.name}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="Members"> Type </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      disabled={showbtn}
                      custom
                      name="reportTypeBy"
                      id="reportTypeBy"
                      onChange={(e) => {
                        console.log(e.target.value);
                        setReportTypeBy(e.target.value);
                        setValue("reportTypeBy", Number(e.target.value));
                      }}
                      onFocus={() => handleTouch("reportTypeBy")}
                    >
                      {otherPrivOptions.map((val) => (
                        <option
                          selected={
                            userEdit && formValues.reportTypeBy == val.value
                          }
                          key={val.value}
                          value={val.value}
                        >
                          {val.name}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>

            <CFormGroup>
              <CRow>
              {showbtn && (
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="Members"> Members </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <MultiSelect
                      options={selectedMembers}
                      //value={selectedMembers}
                      labelledBy="Select"
                      disableSearch={true}
                      hasSelectAll={false}
                    />
                  </CCol>
                </CCol>
              )}
              {!showbtn && (
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="first-name">
                      {" "}
                      {reportTypeBy == 1
                        ? "Select Staffs"
                        : "Select Teams"}{" "}
                    </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <MultiSelect
                      disabled={showbtn}
                      options={staffList}
                      value={userEdit ? selectedMembers : selectedMembers}
                      onChange={setSelecetedMembers}
                      labelledBy="Select"
                    />
                  </CCol>
                </CCol>
              )}
              </CRow>
              
            </CFormGroup>

            {console.log("disabled:", !isFormValid(), hasDifference())}
            {!userEdit && !showbtn && (
              <CButton
                className="submit_btn"
                type="submit"
                // disabled={!isFormValid() || isLoading}
                size="xs"
                color="primary"
              >
                <CIcon name="cil-scrubber" /> Create Report
              </CButton>
            )}
            {!!userEdit && !showbtn && (
              <CButton
                className="submit_btn"
                type="submit"
                //disabled={!isFormValid() || hasDifference() || isLoading}
                size="xs"
                color="primary"
              >
                <CIcon name="cil-scrubber" /> Update Report
              </CButton>
            )}
          </CForm>
        </CCol>
      </CRow>
    </>
  );
};

export default BasicForms;
