import * as yup from "yup";

export const validationSchema = yup.object().shape({
  firstName: yup
    .string("First name has to be string")
    .matches(
      /^([a-zA-Z ]){2,30}$/,
      "first name should only contain alphabets and spaces"
    )
    .required("First Name is Require"),
  lastName: yup
    .string("Last name has to be string")
    .matches(
      /^([a-zA-Z ]){2,30}$/,
      "Last name should only contain alphabets and spaces"
    )
    .required("Last Name is Require"),
  phoneCode: yup.number().required("Phone Code is Require"),
  phoneNumber: yup
    .string("Phone Number has to be number")
    .min(6, "Phone number needs to be of 6 digits")
    .max(6, "Phone number needs to be of 6 digits")
    .required("Phone Number is Require"),
  email: yup
    .string("Email should be string")
    .required("Email is required")
    .email("Email should be valid"),
  roomPrivlege: yup
    .array()
    .min(1, "Room Privlege is required")
    .required("Room Privlege is required"),
  reportPrivlege: yup
    .number()
    .min(1, "Report Privlege is required")
    .required("Report Privlege is required"),
  KPIPrivlege: yup
    .number()
    .min(1, "KPI Privlege is required")
    .required("KPI Privlege is required"),
});
