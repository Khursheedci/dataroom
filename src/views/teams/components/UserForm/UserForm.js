import React, { useContext, useEffect, useState } from "react";
import {
  CButton,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  CRow,
  CTextarea,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import MultiSelect from "react-multi-select-component";
import _ from "lodash";
import "../../../../css/styles.css";

import {
  initialForm,
  phoneCodes,
  roomPrivOptions,
  otherPrivOptions,
  editInitialForm,
} from "./constants";
import { transformForRequest, transformFromRequest } from "./helpers";
import UserServices from "../../../../services/teams";
//import { validationSchema } from "./formValidation";
import { formValidator } from "../../../../libs";
import { ToastContext } from "../../../../context";
import StaffServices from "../../../../services/staffs";
import { element } from "prop-types";

const userService = new UserServices();
const staffService = new StaffServices();

const BasicForms = ({ closeModal, userEdit, showbtn }) => {
  console.log("showbtn is >>>>>>>>>>>>>>>>>>>>>>>", showbtn);
  const [staffList, setStaffList] = useState([]);
  const [selectedMembers, setSelecetedMembers] = useState([]);
  const [formValues, setFormValues] = useState(
    !userEdit
      ? { ...initialForm, addMember: [] }
      : transformFromRequest(userEdit)
  );
  const [selected, setSelected] = useState(
    !userEdit
      ? []
      : roomPrivOptions.filter((priv) =>
          userEdit.addMember.includes(priv.value)
        )
  );

  const [formErrors, setFormErrors] = useState(initialForm);
  const [formTouched, setFormTouched] = useState(
    !userEdit ? initialForm : editInitialForm
  );
  const [isLoading, setLoading] = useState(false);
  const isFormValid = () =>
    Object.values(formValues).every((val) => val !== undefined) &&
    Object.values(formTouched).every((val) => val === true) &&
    Object.values(formErrors).length === 0;
  const { addToast } = useContext(ToastContext);

  // const validateForm = () => {
  //   const errors = formValidator(validationSchema, formValues, formTouched);
  //   setFormErrors(errors);
  // };

  const setValue = (key, value) =>
    setFormValues({ ...formValues, [key]: value });
  const handleTouch = (key) => setFormTouched({ ...formTouched, [key]: true });

  const onFormSubmit = (event) => {
    formValues.addMember = selectedMembers.map((e) => e.value);
    console.log("formvalues are.......", formValues);
    event.preventDefault();
    setLoading(true);
    const body = transformForRequest(formValues);
    if (!userEdit) {
      userService
        .createUser(body)
        .then((res) => closeModal(true))
        .catch((err) => console.log("erss", err))
        .finally(() => setLoading(false));
    } else {
      userService
        .updateUser(userEdit.originalId, body)
        .then((res) => closeModal(true))
        .catch(() => addToast("error"))
        .finally(() => setLoading(false));
    }
  };

  const setPhoneNumber = (value) => {
    if (/^[0-9]{0,6}$/.test(value) && value <= 999999) {
      setValue("phoneNumber", value);
    }
  };

  // useEffect(validateForm, [formValues, selected]);

  useEffect(() => {
    setValue(
      "addMember",
      selected.map((val) => val.value)
    );
    setFormTouched({ ...formTouched, roomPrivlege: true });
  }, [selected]);

  const hasDifference = () => {
    return !userEdit
      ? false
      : _.isEqual(formValues, transformFromRequest(userEdit));
  };

  let options = [];
  let selectedOptions = [];

  useEffect(() => {
    staffService
      .getUsers()
      .then((res) => {
        res.forEach((element) => {
          options.push({
            label:
              element.firstName +
              " " +
              element.lastName +
              " ID-" +
              element.staffId,
            value: element.id,
          });
          formValues.addMember.includes(element.id) &&
            selectedOptions.push({
              label:
                element.firstName +
                " " +
                element.lastName +
                " ID-" +
                element.staffId,
              value: element.id,
              disabled: true,
            });
        });
        setStaffList(options);
        setSelecetedMembers(selectedOptions);
      })
      .catch((err) => console.log(":::", err));

    console.log("formvalues are>>>>", formValues);
    console.log("options are>>>>>>>>", options);
  }, []);

  return (
    <>
      <CRow>
        <CCol lg="12" xs="12">
          <CForm onSubmit={onFormSubmit} className="form-horizontal">
            <CFormGroup row>
              <CCol xs="12" md="9" lg="6">
                <CCol xs="12" md="9" lg="12">
                  <CLabel htmlFor="first-name"> Team Name </CLabel>
                </CCol>
                <CCol xs="12" md="9" lg="12">
                  <CInput
                    disabled={showbtn}
                    id="Team-name"
                    name="Team-name"
                    placeholder="Enter Team Name"
                    value={formValues.teamName}
                    onChange={(e) => setValue("teamName", e.target.value)}
                    onFocus={() => handleTouch("teamName")}
                    className="bg-white text-dark"
                  />
                </CCol>
              </CCol>

              {showbtn && (
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="Members"> Members </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <MultiSelect
                      options={selectedMembers}
                      labelledBy="Select"
                      disableSearch={true}
                      hasSelectAll={false}

                      //onChange={removeMembers}
                    />
                  </CCol>
                </CCol>
              )}
              {!showbtn && (
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="first-name"> Select Staff </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <MultiSelect
                      disabled={showbtn}
                      options={staffList}
                      value={selectedMembers}
                      onChange={setSelecetedMembers}
                      labelledBy="Select"
                      className="bg-white text-dark"
                    />
                  </CCol>
                </CCol>
              )}
            </CFormGroup>
            <CFormGroup>
              <CCol lg="12" xs="12">
                <CLabel htmlFor="last-name"> Description </CLabel>
              </CCol>
              <CCol lg="12" xs="12">
                <CTextarea
                  disabled={showbtn}
                  rows="4"
                  id="last-name"
                  name="last-name"
                  placeholder="Enter Description"
                  value={formValues.description}
                  onChange={(e) => setValue("description", e.target.value)}
                  onFocus={() => handleTouch("description")}
                  className="bg-white text-dark"
                />
              </CCol>
            </CFormGroup>

            {console.log("disabled:", !isFormValid(), hasDifference())}
            {!userEdit && !showbtn && (
              <CButton
                className="submit_btn"
                type="submit"
                // disabled={!isFormValid() || isLoading}
                size="xs"
                color="primary"
              >
                <CIcon name="cil-scrubber" /> Create Team
              </CButton>
            )}
            {!!userEdit && !showbtn && (
              <CButton
                className="submit_btn"
                type="submit"
                //disabled={!isFormValid() || hasDifference() || isLoading}
                size="xs"
                color="primary"
              >
                <CIcon name="cil-scrubber" /> Update Team
              </CButton>
            )}
          </CForm>
        </CCol>
      </CRow>
    </>
  );
};

export default BasicForms;
