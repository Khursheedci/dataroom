export const initialForm = {
  createdAt: undefined,
  description: undefined,
  id: undefined,
  originalId: undefined,
  teamName: undefined,
};

export const editInitialForm = {
  createdAt: true,
  description: true,
  id: true,
  originalId: true,
  teamName: true,
};

export const phoneCodes = [
  23225, 23230, 23231, 23232, 23233, 23234, 23244, 23275, 23276, 23277, 23278,
  23279, 23280, 23288, 23299,
];
export const roomPrivOptions = [
  { label: "Board", value: 1 },
  { label: "Senior Management Cabinet", value: 2 },
  { label: "Departmental Cabinet", value: 3 },
];
export const otherPrivOptions = [
  { name: "Draft", value: 1 },
  { name: "Final", value: 2 },
];
