import React, { useContext, useEffect, useState } from "react";
import {
  CButton,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  CRow,
  CTextarea,
  CInputFile,
  CWidgetProgress,
  CNavLink,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import MultiSelect from "react-multi-select-component";
import _ from "lodash";
import config from "../../../../config/config";

import {
  initialForm,
  dataCabinetsOptions,
  boardCabinetOptions,
  seniorManagementCabinetOptions,
  departmentalCabinetOptions,
  editInitialForm,
} from "./constants";
import { transformForRequest, transformFromRequest } from "./helpers";
import UserServices from "../../../../services/dataRoom";
//import { validationSchema } from "./formValidation";
import { formValidator } from "../../../../libs";
import { ToastContext } from "../../../../context";
import { element } from "prop-types";

const userService = new UserServices();

const BasicForms = ({ closeModal, userEdit, showbtn }) => {
  const [dataCabinetValue, setDataCabinetValue] = useState(
    !userEdit ? 1 : userEdit.dataRoomId
  );
  const [documentFolderValue, setDocumentfolderValue] =
    useState(boardCabinetOptions);
  const [formValues, setFormValues] = useState(
    !userEdit ? { ...initialForm } : transformFromRequest(userEdit)
  );
  const [files, setfile] = useState(undefined);
  const [uploadProgress, setUploadprogress] = useState(0);

  useEffect(() => {
    if (dataCabinetValue == 1) {
      setDocumentfolderValue(boardCabinetOptions);
    }
    if (dataCabinetValue == 2) {
      setDocumentfolderValue(seniorManagementCabinetOptions);
    }
    if (dataCabinetValue == 3) {
      setDocumentfolderValue(departmentalCabinetOptions);
    }
  }, [dataCabinetValue]);

  const [formErrors, setFormErrors] = useState(initialForm);
  const [formTouched, setFormTouched] = useState(
    !userEdit ? initialForm : editInitialForm
  );
  const [isLoading, setLoading] = useState(false);
  const isFormValid = () =>
    Object.values(formValues).every((val) => val !== undefined) &&
    Object.values(formTouched).every((val) => val === true) &&
    Object.values(formErrors).length === 0;
  const { addToast } = useContext(ToastContext);

  const setValue = (key, value) =>
    setFormValues({ ...formValues, [key]: value });
  const handleTouch = (key) => setFormTouched({ ...formTouched, [key]: true });

  const onFormSubmit = (event) => {
    event.preventDefault();
    formValues.dataRoomId = formValues.dataRoomId;
    //parseInt(formValues.dataRoomId);
    formValues.dataRoomPriviledgeId = formValues.dataRoomPriviledgeId;
    //parseInt(formValues.dataRoomPriviledgeId);
    formValues.userId = localStorage.getItem("user-id");
    formValues.DataRoomFile = { value: files[0] };

    setLoading(true);
    const mybody = transformForRequest(formValues);
    let media = {
      uri: files.target.value,
      type: files.target.files[0].type,
      name: files.target.files[0].name,
    };

    const body = new FormData();
    body.append("DataRoomFile", files.target.files[0]);
    body.append("dataRoomId", formValues.dataRoomId);
    body.append("dataRoomPriviledgeId", formValues.dataRoomPriviledgeId);
    body.append("userId", formValues.userId);
    body.append("fileName", formValues.fileName.split(" ").join(""));

    const options = {
      onUploadProgress: (progressEvent) => {
        const { loaded, total } = progressEvent;
        let percent = Math.floor((loaded * 100) / total);
        console.log(`${loaded}kb of ${total}kb | ${percent}%`);
        setUploadprogress(percent);
      },
    };

    // console.log("body>>>>>>>>>>>" , body);

    if (!userEdit) {
      userService
        .createUser(body, options)
        .then((res) => closeModal(true))
        .catch((err) => console.log("erss", err))
        .finally(() => setLoading(false));
    } else {
      userService
        .updateUser(userEdit.originalId, body)
        .then((res) => closeModal(true))
        .catch(() => addToast("error"))
        .finally(() => setLoading(false));
    }
  };

  // useEffect(validateForm, [formValues, selected]);

  const hasDifference = () => {
    return !userEdit
      ? false
      : _.isEqual(formValues, transformFromRequest(userEdit));
  };

  useEffect(() => {
    if (!userEdit) {
      formValues.dataRoomId = 1;
      formValues.dataRoomPriviledgeId = 1;
    } else {
      setDataCabinetValue(userEdit.dataRoomId);
      console.log(">>datacabinet>>>", dataCabinetValue);
    }
  }, []);

  return (
    <>
      <CRow>
        <CCol lg="12" xs="12">
          <CForm onSubmit={onFormSubmit} className="form-horizontal">
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="Members"> Data Cabinets </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      disabled={showbtn}
                      custom
                      name="DataCabinets"
                      id="DataCabinets"
                      className="bg-white text-dark"
                      onChange={(e) => {
                        setValue("dataRoomId", e.target.value);
                        setDataCabinetValue(e.target.value);
                      }}
                      onFocus={() => handleTouch("dataRoomId")}
                      value={formValues.dataRoomId}
                    >
                      {dataCabinetsOptions.map((val) => (
                        <option
                          selected={formValues.dataRoomId == val.value}
                          key={val.value}
                          value={val.value}
                        >
                          {val.name}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>

                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="Members"> Document Folder </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CSelect
                      disabled={showbtn}
                      custom
                      name="DataCabinets"
                      id="DataCabinets"
                      className="bg-white text-dark"
                      onChange={(e) => {
                        setValue("dataRoomPriviledgeId", e.target.value);
                      }}
                      onFocus={() => handleTouch("status")}
                    >
                      {documentFolderValue.map((val) => (
                        <option
                          selected={
                            formValues.dataRoomPriviledgeId == val.value
                          }
                          key={val.value}
                          value={val.value}
                        >
                          {val.name}
                        </option>
                      ))}
                    </CSelect>
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>
            <CFormGroup>
              <CRow>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="first-name"> File Name </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    <CInput
                      disabled={showbtn}
                      id="File-name"
                      name="File-name"
                      placeholder="Enter File Name"
                      value={formValues.fileName}
                      onChange={(e) => setValue("fileName", e.target.value)}
                      onFocus={() => handleTouch("fileName")}
                      className="bg-white text-dark"
                    />
                  </CCol>
                </CCol>
                <CCol xs="12" md="9" lg="6">
                  <CCol xs="12" md="9" lg="12">
                    <CLabel htmlFor="first-name"> File </CLabel>
                  </CCol>
                  <CCol xs="12" md="9" lg="12">
                    {!showbtn && (
                      <CInputFile
                        id="File"
                        name="File"
                        onChange={(e) => {
                          setfile(e);
                          console.log(e);
                          //setValue("DataRoomFile", e.target.files);
                          console.log(e.target.value);
                        }}
                      ></CInputFile>
                    )}
                    {showbtn && (
                      <CButton
                        onClick={() => {
                          console.log(config.url);
                          window.open(
                            config.url +
                              "Download?folderPath=" +
                              formValues.folderPath
                          );
                        }}
                      >
                        {formValues.fileName}
                      </CButton>
                    )}
                    {uploadProgress > 0 && uploadProgress < 100 && (
                      <CWidgetProgress value={uploadProgress} />
                    )}
                  </CCol>
                </CCol>
              </CRow>
            </CFormGroup>

            {console.log("disabled:", !isFormValid(), hasDifference())}
            {!userEdit && !showbtn && (
              <CButton
                className="submit_btn"
                type="submit"
                // disabled={!isFormValid() || isLoading}
                size="xs"
                color="primary"
              >
                <CIcon name="cil-scrubber" /> Upload File
              </CButton>
            )}
            {!!userEdit && !showbtn && (
              <CButton
                className="submit_btn"
                type="submit"
                //disabled={!isFormValid() || hasDifference() || isLoading}
                size="xs"
                color="primary"
              >
                <CIcon name="cil-scrubber" /> Update File
              </CButton>
            )}
          </CForm>
        </CCol>
      </CRow>
    </>
  );
};

export default BasicForms;
