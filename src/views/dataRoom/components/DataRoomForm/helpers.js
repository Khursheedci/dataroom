/**
 * Transform Data for API Specific format
 * @param {*} data
 * @returns
 */
export const transformForRequest = ({ phoneCode, ...rest }) => {
  const transformmedData = {
    ...rest,
    phoneNumber: `${phoneCode}${rest.phoneNumber}`,
  };
  return transformmedData;
};

/**
 * Transform Data from API into form data
 * @param {*} data
 * @returns
 */
export const transformFromRequest = ({
  createdAt,
dataRoomId,
dataRoomPriviledgeId,
fileId,
fileName,
folderPath,
id,
originalId,
DataRoomFile,
}) => {
  const transformmedData = {
    createdAt,
dataRoomId,
dataRoomPriviledgeId,
fileId,
fileName,
folderPath,
id,
originalId,
DataRoomFile,
  };
  console.log(":::>>>>>>>>>>>", transformmedData);
  return transformmedData;
};
