export const initialForm = {
  userId:undefined,
  createdAt: undefined,
dataRoomId: undefined,
dataRoomPriviledgeId: undefined,
fileId: undefined,
fileName: undefined,
folderPath: undefined,
id: undefined,
originalId: undefined,
};

export const editInitialForm = {
  userId:true,
  createdAt: true,
dataRoomId: true,
dataRoomPriviledgeId: true,
fileId: true,
fileName: true,
folderPath: true,
id: true,
originalId: true,
};


export const dataCabinetsOptions = [
  { name: "Board Cabinet", value: 1 },
  { name: "Senior Management Cabinet", value: 2 },
  { name: "Departmental Cabinet", value: 3 },
];
export const boardCabinetOptions = [
  { name: "Board Papers", value: 1 },
  { name: "Organisational Reports", value: 2 },
  { name: "Special Reports", value: 3 },
];
export const seniorManagementCabinetOptions = [
  { name: "Service Contracts", value: 1 },
  { name: "Licenses and Regulatory", value: 2 },
  { name: "Industry Contracts", value: 3 },
  { name: "Departmental Reports", value: 4 },
];
export const departmentalCabinetOptions = [
  { name: "Folder A", value: 1 },
  { name: "Folder B", value: 2 },
  { name: "Folder C", value: 3 },
  { name: "Folder D", value: 4 },
];
