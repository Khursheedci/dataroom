import React, { useEffect, useState } from "react";
import {
  CDataTable,
  CCollapse,
  CCardBody,
  CRow,
  CCol,
  CCallout,
  CBadge,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import { element } from "prop-types";
import { date } from "yup/lib/locale";
import NoItemView from "../../../noItemView/noItemView";
import config from "../../../../config/config";
import {
  dataCabinetsOptions,
  boardCabinetOptions,
  seniorManagementCabinetOptions,
  departmentalCabinetOptions,
} from "../DataRoomForm/constants";

const fields = [
  { key: "fileName", label: "File Name" },
  { key: "dataRoomName", label: "Data Cabinet" },
  { key: "dataRoomPriviledgeName", label: "Document Folder" },
  { key: "Actions", label: "Actions" },
];

const roomPrivOptions = [
  { label: "Board", value: 1 },
  { label: "Senior Management Cabinet", value: 2 },
  { label: "Departmental Cabinet", value: 3 },
];
const otherPriv = {
  1: "Draft",
  2: "Final",
};

const ReportTable = ({ userList, setUserEdit, setUserDelete, setShowbtn }) => {
  const [details, setDetails] = useState([]);
  const [dataCabinet, setDataCabinet] = useState(0);
  const [documentFolder, setDocumentFolder] = useState(0);
  const [docFolOpt, setDocFolOpt] = useState([]);

  console.log("rerenderred");

  const newUserList = [...userList];
  newUserList.forEach((element) => {
    if (element.dataRoomId == 1) {
      element.dataRoomName = "Board";
      if (element.dataRoomPriviledgeId == 1) {
        element.dataRoomPriviledgeName = "Board Papers";
      }
      if (element.dataRoomPriviledgeId == 2) {
        element.dataRoomPriviledgeName = "Organisational Reports";
      }
      if (element.dataRoomPriviledgeId == 3) {
        element.dataRoomPriviledgeName = "Special Reports";
      }
    }
    if (element.dataRoomId == 2) {
      element.dataRoomName = "Senior Management Cabinet";
      if (element.dataRoomPriviledgeId == 1) {
        element.dataRoomPriviledgeName = "Service Contracts";
      }
      if (element.dataRoomPriviledgeId == 2) {
        element.dataRoomPriviledgeName = "Licenses and Regulatory";
      }
      if (element.dataRoomPriviledgeId == 3) {
        element.dataRoomPriviledgeName = "Industry Contracts";
      }
      if (element.dataRoomPriviledgeId == 4) {
        element.dataRoomPriviledgeName = "Departmental Reports";
      }
    }
    if (element.dataRoomId == 3) {
      element.dataRoomName = "Departmental Cabinet";
      if (element.dataRoomPriviledgeId == 1) {
        element.dataRoomPriviledgeName = "Folder A";
      }
      if (element.dataRoomPriviledgeId == 2) {
        element.dataRoomPriviledgeName = "Folder B";
      }
      if (element.dataRoomPriviledgeId == 3) {
        element.dataRoomPriviledgeName = "Folder C";
      }
      if (element.dataRoomPriviledgeId == 4) {
        element.dataRoomPriviledgeName = "Folder D";
      }
    }
  });

  const [filteredList, setFilteredList] = useState(newUserList);

  useEffect(() => {
    console.log("docfolder value >>>>>>>>", documentFolder, " & ", dataCabinet);
    setFilteredList([...newUserList]);
    let lisst = [...newUserList];
    if (dataCabinet == 0) {
      lisst = [...newUserList];
      setDocFolOpt([]);
    } else {
      lisst = [...newUserList];
      lisst = lisst.filter((e) => e.dataRoomId == dataCabinet);
      if (dataCabinet == 1) {
        setDocFolOpt([...boardCabinetOptions]);
      } else if (dataCabinet == 2) {
        setDocFolOpt([...seniorManagementCabinetOptions]);
      } else if (dataCabinet == 3) {
        setDocFolOpt([...departmentalCabinetOptions]);
      } else {
        setDocFolOpt([]);
      }
    }

    if (documentFolder !== 0) {
      lisst = lisst.filter((e) => e.dataRoomPriviledgeId == documentFolder);
    }

    setFilteredList(lisst);
  }, [dataCabinet, documentFolder, userList]);
  const showProfile = (item) => {
    setShowbtn(true);
    setUserEdit(item);
  };
  const showEdit = (item) => {
    setShowbtn(false);
    setUserEdit(item);
  };

  const toggleDetails = ({ originalId: id }) => {
    const position = details.indexOf(id);
    let newDetails = details.slice();
    if (position !== -1) {
      newDetails.splice(position, 1);
    } else {
      newDetails.push(id);
    }
    setDetails(newDetails);
  };

  const downloadFile = (item) => {
    window.open(config.url + "Download?folderPath=" + item.folderPath);
  };

  const actions = [
    { icon: freeSet.cilLifeRing, onClick: showProfile },
    { icon: freeSet.cilPenAlt, onClick: showEdit },
    { icon: freeSet.cilUserX, onClick: setUserDelete },
    {
      icon: freeSet.cilArrowThickToBottom,
      onClick: downloadFile,
    },
  ];

  return (
    <>
      <CRow>
        <CSelect
          name="DataCabinets"
          id="DataCabinets"
          className="bg-white text-dark"
          onChange={(e) => {
            setDocumentFolder(0);
            setDataCabinet(e.target.value);
          }}
          className="col-sm-6 form-inline p-0"
        >
          <option value={0}>Select Data Cabinet</option>
          {dataCabinetsOptions.map((val) => (
            <option key={val.value} value={val.value}>
              {val.name}
            </option>
          ))}
        </CSelect>

        <CSelect
          name="DocumentFolder"
          id="DocumentFolder"
          className="bg-white text-dark"
          onChange={(e) => {
            setDocumentFolder(e.target.value);
          }}
          className="col-sm-6 form-inline p-0"
        >
          <option key={0} value={0}>
            Select Document Folder
          </option>
          {docFolOpt.map((val) => (
            <option key={val.value} value={val.value}>
              {val.name}
            </option>
          ))}
        </CSelect>
      </CRow>
      <CDataTable
        bordered={true}
        items={filteredList}
        fields={fields}
        itemsPerPage={5}
        pagination
        tableFilter={{ placeholder: "search..." }}
        noItemsViewSlot={<NoItemView />}
        scopedSlots={{
          Actions: (item) => (
            <div>
              {actions.map((action) => (
                <td>
                  <CIcon
                    size={"lg"}
                    content={action.icon}
                    onClick={() => action.onClick(item)}
                  />
                </td>
              ))}
            </div>
          ),
        }}
      />
    </>
  );
};

export default ReportTable;
