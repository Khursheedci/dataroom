import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { userApi } from '../config';
// m8f4mws8jxrdgro8ul60rh
const UserContext = React.createContext({});
const UserDetails = (props) => {
  const [userInfo, setUserInfo] = useState(undefined);
  const [checking, setChecking] = useState(true);

  const login = async (credentials) => {
    try {
      let { data: { data: { token, userInfo } } } = await axios.post(userApi.login, credentials);
      console.log("::::", token, userInfo)
      localStorage.setItem('user-token', token);
      setUserInfo(userInfo);
      return true;
    } catch (err) {
      console.log(":::", err);
      if (err.message === 'Request failed with status code 401') {
        clearSession();
      }
      return false
    }
  }

  const clearSession = () => {
    localStorage.removeItem('user-token');
    setUserInfo(undefined);
  }

  const getUserToken = () => {
    return localStorage.getItem('user-token') || '';
  }

  const validateLogin = async () => {
    try {
      if (getUserToken()) {
        let { data: { data: userInfo } } = await axios.get(userApi.verify, getAuthHeader());
        console.log(';;;', userInfo)
        setUserInfo(userInfo)
      } else {
        clearSession()
      }
    } catch (err) {
      if (err.message === 'Request failed with status code 401') {
        clearSession();
      }
    } finally {
      setChecking(false)
    }
  }

  const getAuthHeader = () => {
    return { headers: { Authorization: 'Bearer ' + getUserToken() } };
  }

  const logOut = async () => {
    try {
      // await axios.post(config.url + 'logout', {}, getUserToken());
      clearSession();
    } catch (err) {
      // TODO: Think of something
    }
  }

  useEffect(() => {
    validateLogin();
  }, []);

  const { children } = props;
  return (
    <UserContext.Provider value={{
      login,
      logOut,
      getAuthHeader,
      userInfo,
      checking
    }}>
      {children}
    </UserContext.Provider>
  )
}

export { UserContext, UserDetails };