import React, { useState } from 'react';
import {
  CToaster,
  CToast,
  CToastHeader,
  CToastBody
} from '@coreui/react';

const ToastContext = React.createContext();
const Toaster = ({ children }) => {
  const [toasts, setToasts] = useState([]);

  const addToast = ({ message, position = 'top-right', autohide = 3000, fade = true }) => {
    setToasts([
      ...toasts,
      { position, autohide, fade }
    ])
  }


  const toasters = (() => {
    return toasts.reduce((toasters, toast) => {
      toasters[toast.position] = toasters[toast.position] || []
      toasters[toast.position].push(toast)
      return toasters
    }, {})
  })()

  return (
    <>
      <ToastContext.Provider value={{ addToast }}>
        {children}
      </ToastContext.Provider>
      {Object.keys(toasters).map((toasterKey) => (
        <CToaster
          position={toasterKey}
          key={'toaster' + toasterKey}
        >
          {
            toasters[toasterKey].map((toast, key) => {
              return (
                <CToast
                  key={'toast' + key}
                  show={true}
                  autohide={toast.autohide}
                  fade={toast.fade}
                >
                  <CToastHeader>
                    Toast title
                  </CToastHeader>
                  <CToastBody>
                    {`This is a toast in ${toasterKey} positioned toaster number ${key + 1}.${toast.value}`}
                  </CToastBody>
                </CToast>
              )
            })
          }
        </CToaster>
      ))}
    </>
  )

}

export { Toaster, ToastContext };