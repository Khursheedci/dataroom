import config from "./config";

export const userApi = {
  register: `${config.url}users/register`,
  get: `${config.url}users`,
  list: `${config.url}users/list`,
  update: `${config.url}users`,
  delete: `${config.url}users`,
  login: `${config.url}users/login`,
  verify: `${config.url}users/verify`,
};

export const staffApi = {
  register: `${config.url}staff/register`,
  get: `${config.url}staff`,
  list: `${config.url}staff/list`,
  update: `${config.url}staff`,
  delete: `${config.url}staff`,
  login: `${config.url}staff/login`,
  verify: `${config.url}staff/verify`,
};

export const teamApi = {
  register: `${config.url}team/register`,
  get: `${config.url}team`,
  list: `${config.url}team/list`,
  update: `${config.url}team`,
  delete: `${config.url}team`,
  login: `${config.url}team/login`,
  verify: `${config.url}team/verify`,
};
export const activityApi = {
  get: `${config.url}activity`,
  list: `${config.url}activity/list`,
  update: `${config.url}activity`,
  delete: `${config.url}activity/`,
  create: `${config.url}activity/register`,
};

export const reportApi = {
  register: `${config.url}report/register`,
  get: `${config.url}report`,
  list: `${config.url}report/list`,
  update: `${config.url}report`,
  delete: `${config.url}report`,
  login: `${config.url}report/login`,
  verify: `${config.url}report/verify`,
};

export const dataRoomApi = {
  register: `${config.url}dataRoom/register`,
  get: `${config.url}dataRoom`,
  list: `${config.url}dataRoom/list`,
  update: `${config.url}dataRoom`,
  delete: `${config.url}dataRoom`,
  login: `${config.url}dataRoom/login`,
  verify: `${config.url}dataRoom/verify`,
};