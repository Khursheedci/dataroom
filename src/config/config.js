  
const envVars = process.env;

export default Object.freeze({
  url: `${envVars.REACT_APP_SERVICE_URL}/api/`,
})