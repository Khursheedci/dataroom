export const formValidator = (validationSchema, formValues, formTouched) => {
  try {
    const result = validationSchema.validateSync(formValues, {
      abortEarly: false,
    });
    return {};
  } catch (err) {
    const errors = err.inner.reduce((updatedErr, { path, message }) => {
      if (formTouched[path]) {
        updatedErr[path] = message;
      }
      return updatedErr;
    }, {});
    return errors;
    //setFormErrors(errors);
  }
};
