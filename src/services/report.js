import axios from "axios";
import { reportApi } from "../config";

class StaffServices {
  constructor() {
    this.token = localStorage.getItem("user-token");
  }

  getHeaders() {
    return { headers: { authorization: `Bearer ${this.token}` } };
  }

  createUser(userdata) {
    return axios
      .post(reportApi.register, userdata, this.getHeaders())
      .then((res) => res.data);
  }

  getUser(id) {
    return axios
      .get(`${reportApi.get}/${id}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  getUsers() {
    return axios
      .get(`${reportApi.list}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  updateUser(id, body) {
    return axios
      .put(`${reportApi.update}/${id}`, body, this.getHeaders())
      .then((res) => res.data);
  }

  deleteUser(id) {
    return axios
      .delete(`${reportApi.delete}/${id}`, this.getHeaders())
      .then((res) => res.data);
  }
}

export default StaffServices;
