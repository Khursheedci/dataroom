import axios from 'axios';
import { userApi } from '../config';

class UserServices {
  constructor() {
    this.token = localStorage.getItem('user-token')
  }

  getHeaders () {
    return { headers: { authorization: `Bearer ${this.token}` } };
  }

  createUser(userdata) {
    return axios.post(userApi.register, userdata, this.getHeaders())
      .then(res => res.data)
  }

  getUser(id) {
    return axios.get(`${userApi.get}/${id}`, this.getHeaders())
      .then(res => res.data.data)
  }

  getUsers() {
    return axios.get(`${userApi.list}`, this.getHeaders())
      .then(res => res.data.data)
  }

  updateUser(id, body) {
    return axios.put(`${userApi.update}/${id}`, body, this.getHeaders())
      .then(res => res.data)
  }

  deleteUser(id) {
    return axios.delete(`${userApi.delete}/${id}`, this.getHeaders())
      .then(res => res.data)
  }
}

export default UserServices;