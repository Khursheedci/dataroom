import axios from "axios";
import { teamApi } from "../config";

class TeamServices {
  constructor() {
    this.token = localStorage.getItem("user-token");
  }

  getHeaders() {
    return { headers: { authorization: `Bearer ${this.token}` } };
  }

  createUser(userdata) {
    return axios
      .post(teamApi.register, userdata, this.getHeaders())
      .then((res) => res.data);
  }

  getUser(id) {
    return axios
      .get(`${teamApi.get}/${id}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  getUsers() {
    return axios.get(`${teamApi.list}`, this.getHeaders()).then((res) => {
      console.log(res);
      return res.data.data;
    });
  }

  updateUser(id, body) {
    return axios
      .put(`${teamApi.update}/${id}`, body, this.getHeaders())
      .then((res) => res.data);
  }

  deleteUser(id) {
    return axios
      .delete(`${teamApi.delete}/${id}`, this.getHeaders())
      .then((res) => res.data);
  }
}

export default TeamServices;
