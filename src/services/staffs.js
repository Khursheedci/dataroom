import axios from "axios";
import { staffApi } from "../config";

class StaffServices {
  constructor() {
    this.token = localStorage.getItem("user-token");
  }

  getHeaders() {
    return { headers: { authorization: `Bearer ${this.token}` } };
  }

  createUser(userdata) {
    return axios
      .post(staffApi.register, userdata, this.getHeaders())
      .then((res) => res.data);
  }

  getUser(id) {
    return axios
      .get(`${staffApi.get}/${id}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  getUsers() {
    return axios
      .get(`${staffApi.list}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  updateUser(id, body) {
    return axios
      .put(`${staffApi.update}/${id}`, body, this.getHeaders())
      .then((res) => res.data);
  }

  deleteUser(id) {
    return axios
      .delete(`${staffApi.delete}/${id}`, this.getHeaders())
      .then((res) => res.data);
  }
}

export default StaffServices;
