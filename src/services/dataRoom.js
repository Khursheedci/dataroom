import axios from "axios";
import { dataRoomApi } from "../config";

class StaffServices {
  constructor() {
    this.token = localStorage.getItem("user-token");
  }

  getHeaders() {
    return { headers: { authorization: `Bearer ${this.token}` } };
  }

  createUser(userdata, options) {
    options.headers = {
      authorization: `Bearer ${this.token}`,
      "Content-Type": `multipart/form-data; boundary=${userdata._boundary}`,
    };
    return axios
      .post(dataRoomApi.register, userdata, options)
      .then((res) => res.data);
  }

  getUser(id) {
    return axios
      .get(`${dataRoomApi.get}/${id}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  getUsers() {
    return axios
      .get(`${dataRoomApi.list}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  updateUser(id, body) {
    return axios
      .put(`${dataRoomApi.update}/${id}`, body, this.getHeaders())
      .then((res) => res.data);
  }

  deleteUser(id) {
    return axios
      .delete(`${dataRoomApi.delete}/${id}`, this.getHeaders())
      .then((res) => res.data);
  }
}

export default StaffServices;
