import axios from "axios";
import { activityApi } from "../config";

class ActivityServices {
  constructor() {
    this.token = localStorage.getItem("user-token");
  }

  getHeaders() {
    return { headers: { authorization: `Bearer ${this.token}` } };
  }

  createActivity(activityData) {
    return axios
      .post(activityApi.create, activityData, this.getHeaders())
      .then((res) => res.data);
  }

  getActivity(id) {
    console.log(id);
    return axios
      .get(`${activityApi.get}/${id}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  getActivities() {
    return axios
      .get(`${activityApi.list}`, this.getHeaders())
      .then((res) => res.data.data);
  }

  updateActivity(id, body) {
    return axios
      .put(`${activityApi.update}/${id}`, body, this.getHeaders())
      .then((res) => res.data);
  }

  deleteActivity(id) {
    return axios
      .delete(`${activityApi.delete}/${id}`, this.getHeaders())
      .then((res) => res.data);
  }
}

export default ActivityServices;
