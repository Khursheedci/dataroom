import React from 'react';
import { CSpinner, CRow, CCol } from '@coreui/react'

const Spinner = () => (
  <CRow>
    <CCol xl="6" md="6" sm="6" md="6" />
    <CCol xl="6" md="6" sm="6" md="6">
      <CSpinner color="primary" />
    </CCol>
  </CRow>
)

export default Spinner;