import React from "react";
import { Redirect, Route } from 'react-router-dom';
import { UserContext } from '../context';

export const PublicRoute = ({ component: Component, ...rest }) => (
  <UserContext.Consumer>
    {({ userInfo }) => {
      return (
        <>
          <Route
            {...rest}
            render={props => {
              return (!userInfo ? (
                <Component {...props} />
              ) : (
                <Redirect
                  to={{
                    pathname: "/",
                    state: { from: props.location }
                  }}
                />
              ))
            }
            }
          />
        </>
      )
    }}
  </UserContext.Consumer>
)

export default PublicRoute;