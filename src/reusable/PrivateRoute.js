import React from "react";
import { Redirect, Route, withRouter } from 'react-router-dom';
import { UserContext } from '../context';

export const PrivateRoute = ({ component, location, ...rest }) => (
  <UserContext.Consumer>
    {({ userInfo, checking }) => {
      if (!userInfo && checking) {
        return <></>
      }
      return (
        <>
          {
            userInfo ? (
              <Route
                {...rest}
                component={component}
              />
            ) : (
              <Redirect
                to={{
                  pathname: '/login',
                  state: { from: location.pathname }
                }}
              />
            )}
        </>
      )
    }}
  </UserContext.Consumer>
)

export default withRouter(PrivateRoute);