import React from "react";

const Toaster = React.lazy(() =>
  import("./views/notifications/toaster/Toaster")
);

const Charts = React.lazy(() => import("./views/charts/Charts"));
const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));
const CoreUIIcons = React.lazy(() =>
  import("./views/icons/coreui-icons/CoreUIIcons")
);
const Flags = React.lazy(() => import("./views/icons/flags/Flags"));
const Brands = React.lazy(() => import("./views/icons/brands/Brands"));
const Alerts = React.lazy(() => import("./views/notifications/alerts/Alerts"));
const Badges = React.lazy(() => import("./views/notifications/badges/Badges"));
const Modals = React.lazy(() => import("./views/notifications/modals/Modals"));
const Colors = React.lazy(() => import("./views/theme/colors/Colors"));
// const Typography = React.lazy(() => import('./views/theme/typography/Typography'));
const Widgets = React.lazy(() => import("./views/widgets/Widgets"));
const Users = React.lazy(() => import("./views/users/UserScreen"));
// const User = React.lazy(() => import('./views/users/User'));

const Staffs = React.lazy(() => import("./views/staffs/staffScreen"));
const Staff = React.lazy(() => import("./views/staffs/Staff"));

const Teams = React.lazy(() => import("./views/teams/UserScreen"));
const Team = React.lazy(() => import("./views/teams/UserProfile"));

const Reports = React.lazy(() => import("./views/report/ReportScreen"));
const DataRoom = React.lazy(() => import("./views/dataRoom/DataRoomScreen"));
// const Report = React.lazy(() => import("./views/reports/Report"));
const Activities = React.lazy(() => import("./views/Activities/Activities"));
const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/theme", name: "Theme", component: Colors, exact: true },
  { path: "/theme/colors", name: "Colors", component: Colors },
  // { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: "/charts", name: "Charts", component: Charts },
  { path: "/icons", exact: true, name: "Icons", component: CoreUIIcons },
  { path: "/icons/coreui-icons", name: "CoreUI Icons", component: CoreUIIcons },
  { path: "/icons/flags", name: "Flags", component: Flags },
  { path: "/icons/brands", name: "Brands", component: Brands },
  {
    path: "/notifications",
    name: "Notifications",
    component: Alerts,
    exact: true,
  },
  { path: "/notifications/alerts", name: "Alerts", component: Alerts },
  { path: "/notifications/badges", name: "Badges", component: Badges },
  { path: "/notifications/modals", name: "Modals", component: Modals },
  { path: "/notifications/toaster", name: "Toaster", component: Toaster },
  { path: "/widgets", name: "Widgets", component: Widgets },
  {
    path: "/users",
    exact: true,
    name: "Users",
    component: Users,
    authLevel: "admin",
    requireAdmin: true,
  },
  // { path: '/users/:id', exact: true, name: 'User Details', component: User, authLevel: "admin", requireAdmin: true },
  { path: "/staffs", exact: true, name: "Staffs", component: Staffs },
  { path: "/teams", exact: true, name: "Teams", component: Teams },
  {
    path: "/Reports",
    exact: true,
    name: "Reports",
    component: Reports,
    // authLevel: "roomPrivilege",
    // roomPrivilegeRequired: 3,
  }, {
    path: "/activities",
    exact: true,
    name: "Activities",
    component: Activities
  },
{
    path: "/DataRoom",
    exact: true,
    name: "DataRoom",
    component: DataRoom
  }
];

export default routes;
